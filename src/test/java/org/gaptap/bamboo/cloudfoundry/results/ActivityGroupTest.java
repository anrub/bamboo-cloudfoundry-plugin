/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.results;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
public class ActivityGroupTest {
	
//	year - the year
//	monthOfYear - the month of the year, from 1 to 12
//	dayOfMonth - the day of the month, from 1 to 31
//	hourOfDay - the hour of the day, from 0 to 23
//	minuteOfHour - the minute of the hour, from 0 to 59
//	secondOfMinute - the second of the minute, from 0 to 59
//	millisOfSecond - the millisecond of the second, from 0 to 999
	
	private Chain chain;
	private Job job;
	
	@Before
	public void setup(){
		 chain = mock(Chain.class, RETURNS_DEEP_STUBS);
		when(chain.getBuildName()).thenReturn("Bamboo Test Plan");
		when(chain.getProject().getName()).thenReturn("My Project");
		
		 job = mock(Job.class, RETURNS_DEEP_STUBS);
		when(job.getParent()).thenReturn(chain);
	}

	@Test
	public void activitiesAreGroupedByHourWhenMappingFromResultSummaryLists(){

		List<ResultsSummary> results = Lists.newArrayList();
		results.add(createResult(new DateTime(2013, 2, 1, 23, 30, 0, 0)));
		results.add(createResult(new DateTime(2013, 2, 1, 23, 35, 0, 0)));
		results.add(createResult(new DateTime(2013, 2, 2, 12, 15, 0, 0)));
		results.add(createResult(new DateTime(2013, 2, 2, 12, 25, 0, 0)));
		results.add(createResult(new DateTime(2013, 2, 2, 12, 55, 0, 0)));
		results.add(createResult(new DateTime(2013, 2, 2, 13, 15, 0, 0)));
		
		List<ActivityGroup> groups = ActivityGroup.from(results);
		
		assertThat(groups.size(), is(3));
	}

	private ResultsSummary createResult(DateTime dateTime) {
		ResultsSummary result = mock(ResultsSummary.class, RETURNS_DEEP_STUBS);
		when(result.getPlan()).thenReturn(job);
		
		PlanResultKey planResultKey = PlanKeys.getPlanResultKey("MY-JOB-123");
		when(result.getPlanResultKey()).thenReturn(planResultKey);
		
		when(result.getBuildDate()).thenReturn(dateTime.toDate());
		return result;
	}
}
