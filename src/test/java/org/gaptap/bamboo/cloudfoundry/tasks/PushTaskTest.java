/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.DISABLE_FOR_BUILD_PLANS;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.NullBuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import com.google.common.collect.Maps;

/**
 * @author David Ehringer
 */
public class PushTaskTest {

    @Mock
    private EncryptionService encryptionService;
    @Mock
    private TaskContext taskContext;

    private BuildLogger buildLogger = new NullBuildLogger();
    private ConfigurationMap configurationMap;
    private Map<String, String> runtimeTaskContext;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        configurationMap = new ConfigurationMapImpl();
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        runtimeTaskContext = Maps.newHashMap();
        when(taskContext.getRuntimeTaskContext())
                .thenReturn(runtimeTaskContext);

        when(taskContext.getBuildLogger()).thenReturn(buildLogger);
    }

    @Test
    public void whenDisableForBuildPlansIsSelectedAndTheTaskExecutesInABuildPlanTheTaskFails()
            throws TaskException {
        // Given
        PushTask task = new PushTask(encryptionService);
        runtimeTaskContext.put(DISABLE_FOR_BUILD_PLANS, "true");

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.ERROR));
    }
}
