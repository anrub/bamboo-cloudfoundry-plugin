/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.results;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.google.common.collect.Lists;


/**
 * Homegrown activity stream solution until we can hook directly into activity streams in Bamboo.
 * 
 * @author David Ehringer
 */
public class ActivityGroup {

	private Date date;
	private List<Activity> activities = Lists.newArrayList();

	public Date getDate() {
		return date;
	}
	
	public String getDateDescription(){
		DateFormat formatter = new SimpleDateFormat("MMMMM d - h a");
		return formatter.format(date);
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public void addActivities(List<Activity> activities) {
		this.activities.addAll(activities);
	}

	public void addActivity(Activity activity) {
		this.activities.add(activity);
	}

	public static List<ActivityGroup> from(List<ResultsSummary> results) {
		sort(results);
		List<ActivityGroup> groups = Lists.newArrayList();
		ActivityGroup currentGroup = null;
		for (ResultsSummary result : results) {
			Activity activity = Activity.from(result);
			if (currentGroup == null || !sameGroup(currentGroup.getDate(), result.getBuildDate())) {
				currentGroup = new ActivityGroup();
				groups.add(currentGroup);
				currentGroup.setDate(result.getBuildDate());
				currentGroup.addActivity(activity);
			} else {
				currentGroup.addActivity(activity);
			}
		}
		return groups;
	}

	private static void sort(List<ResultsSummary> results) {
		Collections.sort(results, new Comparator<ResultsSummary>() {
			@Override
			public int compare(ResultsSummary r1, ResultsSummary r2) {
				return r2.getBuildDate().compareTo(r1.getBuildDate());
			}
		});
	}

	private static boolean sameGroup(Date d1, Date d2) {
		DateTime dt1 = round(new DateTime(d1));
		DateTime dt2 = round(new DateTime(d2));
		return dt1.compareTo(dt2) == 0;
	}

	private static DateTime round(DateTime d) {
		return d.hourOfDay().roundFloorCopy();
	}

}
