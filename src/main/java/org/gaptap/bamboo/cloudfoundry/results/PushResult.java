/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.results;

import static com.google.common.collect.Maps.filterEntries;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.TARGET_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_MANUAL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_YAML;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.COMMAND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ENVIRONMENT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.INSTANCES;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.MEMORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SELECTED_APP_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.URIS;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TARGET_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.USERNAME;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import org.apache.commons.lang.Validate;
import org.gaptap.bamboo.cloudfoundry.results.processors.DeploymentResultProcessor;
import org.gaptap.bamboo.cloudfoundry.results.processors.TaskExecutedPostBuildIndexWriter;
import org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider;

import com.atlassian.bamboo.task.RuntimeTaskContext;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskResult;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author David Ehringer
 * @see DeploymentResultProcessor
 * @see TaskExecutedPostBuildIndexWriter
 */
public class PushResult {

	public static final String CLOUDFOUNDRY_PUSH_TASK_VALUE = "true";
	public static final String CLOUDFOUNDRY_PUSH_TASK_KEY = "cloudfoundry.task.push";
	public static final String TARGET_NAME = "targetName";
	public static final String TASK_STATE = "taskState";
	public static final String TASK_ID = "taskId";
	public static final String MANIFEST_CONTENTS = "manifestContents";

	public static final String KEY_PREFIX = "cloudfoundry.push.";
	private static final char SEPARATOR = '.';

	private Map<String, String> taskData = Maps.newHashMap();

	public static PushResult from(TaskDefinition taskDef,RuntimeTaskContext runtimeTaskContext, TaskResult taskResult) {
		Validate.isTrue(taskDef.getId() == taskResult.getTaskIdentifier().getId());
		PushResult result = new PushResult();
		result.taskData.put(TASK_ID, String.valueOf(taskDef.getId()));
		result.taskData.put(TASK_STATE, taskResult.getTaskState().name());
		result.taskData.put(TARGET_URL, runtimeTaskContext.getRuntimeContextForTask(taskDef).get(TARGET_URL));
        result.taskData.put(USERNAME, runtimeTaskContext.getRuntimeContextForTask(taskDef).get(USERNAME));

		Map<String, String> taskConfig = taskDef.getConfiguration();
		copyEntries(taskConfig, result.taskData, TARGET_ID, START, SELECTED_APP_CONFIG_OPTION);

		// It is easier to copy everything in the taskConfig but there is a lot
		// of stuff in there that
		// is defaulted and not used depending on the scenario. If we put it in
		// there it ends up on the Metadata
		// tab in the build results and it can be misleading.
		if (APP_CONFIG_OPTION_MANUAL.equals(taskConfig.get(SELECTED_APP_CONFIG_OPTION))) {
			copyEntries(taskConfig, result.taskData, APPLICATION_NAME, URIS, MEMORY, INSTANCES,
					ENVIRONMENT, COMMAND);
			if (FILE.equals(taskConfig.get(APP_LOCATION))) {
				copyEntries(taskConfig, result.taskData, FILE);
			} else if (DIRECTORY.equals(taskConfig.get(APP_LOCATION))) {
				copyEntries(taskConfig, result.taskData, DIRECTORY);
			}
		}

		return result;
	}

	private static void copyEntries(Map<String, String> source, Map<String, String> target, String... keys) {
		for (String key : keys) {
			target.put(key, source.get(key));
		}
	}

	public Map<String, String> toCustomBuildData() {
		Map<String, String> buildData = Maps.newHashMap();
		for (Entry<String, String> entry : taskData.entrySet()) {
			String customBuildDataKey = createCustomBuildDataKey(entry.getKey(), getTaskId());
			buildData.put(customBuildDataKey, entry.getValue());
		}
		
		// Adding this key allows us to easily find push tasks later
		buildData.put(CLOUDFOUNDRY_PUSH_TASK_KEY, CLOUDFOUNDRY_PUSH_TASK_VALUE);
		return buildData;
	}

	private static String createCustomBuildDataKey(String pushResultKey, String taskId) {
		return KEY_PREFIX + taskId + SEPARATOR + pushResultKey;
	}

	private static String extractPushResultKey(String customBuildDataKey) {
		String suffix = customBuildDataKey.substring(KEY_PREFIX.length());
		int dot = suffix.indexOf(SEPARATOR);
		return suffix.substring(dot + 1);
	}

	private static String extractTaskId(String customBuildDataKey) {
		String suffix = customBuildDataKey.substring(KEY_PREFIX.length());
		int dot = suffix.indexOf(SEPARATOR);
		if(dot == -1){
			return null;
		}
		return suffix.substring(0, dot);
	}

	public static List<PushResult> fromCustomBuildData(Map<String, String> buildData) {
		Map<String, PushResult> results = Maps.newHashMap();
		for (Entry<String, String> entry : filterEntries(buildData, isPushResultEntry()).entrySet()) {
			String key = entry.getKey();
			String taskId = extractTaskId(key);
			if(taskId == null){
				continue;
			}
			PushResult result = null;
			if (!results.containsKey(taskId)) {
				result = new PushResult();
				results.put(taskId, result);
			} else {
				result = results.get(taskId);
			}
			result.taskData.put(extractPushResultKey(key), entry.getValue());
		}
		return Lists.newArrayList(results.values());
	}

	public static void addManifestContetsToCustomBuildData(String manifestContents, TaskDefinition taskDef,
			Map<String, String> buildData) {
		String taskId = String.valueOf(taskDef.getId());
		buildData.put(createCustomBuildDataKey(MANIFEST_CONTENTS, taskId), manifestContents);
	}

	public String getTaskId() {
		return taskData.get(TASK_ID);
	}

	public String getTaskState() {
		return taskData.get(TASK_STATE);
	}

	public String getTargetUrl() {
		return taskData.get(TargetTaskDataProvider.TARGET_URL);
	}

	public String getConfigOption() {
		return taskData.get(SELECTED_APP_CONFIG_OPTION);
	}

	public boolean isManifestConfig() {
		return APP_CONFIG_OPTION_YAML.equals(getConfigOption());
	}

	public String getManifestContents() {
		return taskData.get(MANIFEST_CONTENTS);
	}

	public String getApplicationName() {
		return taskData.get(APPLICATION_NAME);
	}

	public String getInstances() {
		return taskData.get(INSTANCES);
	}

	public List<String> getUrls() {
		String urlList = taskData.get(URIS);
		return Lists.newArrayList(urlList.split(","));
	}

	public static Predicate<Entry<String, String>> isPushResultEntry() {
		return new Predicate<Entry<String, String>>() {
			@Override
			public boolean apply(@Nullable Entry<String, String> entry) {
				if (entry.getKey().startsWith(KEY_PREFIX)) {
					return true;
				}
				return false;
			}
		};
	}
}
