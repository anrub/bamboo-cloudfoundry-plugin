/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.results.processors;

import java.util.Map;

import org.apache.lucene.document.Document;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.index.CustomPostBuildIndexWriter;
import com.atlassian.bamboo.index.IndexUtils;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import org.gaptap.bamboo.cloudfoundry.results.PushResult;

/**
 * Adds a term to the Lucene index that allows us to quickly and easily locate
 * {@link ResultsSummary}s that have Cloud Foundry tasks.
 * <em>Note: there is tight coupling between this class, {@link PushResult}, and {@link DeploymentResultProcessor}.</em>
 * 
 * @author David Ehringer
 * @see PushResult
 * @see TaskExecutedPostBuildIndexWriter
 */
public class TaskExecutedPostBuildIndexWriter implements CustomPostBuildIndexWriter {

	@Override
	public void updateIndexDocument(@NotNull Document document, @NotNull BuildResultsSummary buildResult) {
		Map<String, String> buildData = buildResult.getCustomBuildData();
		if (PushResult.CLOUDFOUNDRY_PUSH_TASK_VALUE.equals(buildData.get(PushResult.CLOUDFOUNDRY_PUSH_TASK_KEY))) {
			IndexUtils.addField(PushResult.CLOUDFOUNDRY_PUSH_TASK_KEY, PushResult.CLOUDFOUNDRY_PUSH_TASK_VALUE,
					document);
		}
	}
}
