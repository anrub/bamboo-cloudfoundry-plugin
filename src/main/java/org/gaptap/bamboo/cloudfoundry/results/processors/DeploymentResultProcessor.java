/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.results.processors;

import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isCloudFoundryPushTask;
import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isCloudFoundryTaskResult;
import static com.google.common.collect.Iterables.filter;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.CustomBuildProcessorServer;
import com.atlassian.bamboo.task.RuntimeTaskContext;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import org.gaptap.bamboo.cloudfoundry.results.PushResult;

/**
 * @author David Ehringer
 * 
 */
public class DeploymentResultProcessor implements CustomBuildProcessorServer {

	private BuildContext buildContext;

	@Override
	public void init(@NotNull BuildContext buildContext) {
		this.buildContext = buildContext;
	}

	@Override
	@NotNull
	public BuildContext call() throws InterruptedException, Exception {

		CurrentBuildResult buildResult = buildContext.getBuildResult();
		RuntimeTaskContext runtimeTaskContext = buildContext.getRuntimeTaskContext();

		for (TaskResult taskResult : filter(buildResult.getTaskResults(), isCloudFoundryTaskResult())) {
			for (TaskDefinition taskDef : filter(buildContext.getBuildDefinition().getTaskDefinitions(),
					isCloudFoundryPushTask())) {
				if (taskResult.getTaskIdentifier().getId() == taskDef.getId()) {
					PushResult pushResult = PushResult.from(taskDef, runtimeTaskContext, taskResult);
					buildResult.getCustomBuildData().putAll(pushResult.toCustomBuildData());
				}
			}
		}
		return buildContext;
	}

}
