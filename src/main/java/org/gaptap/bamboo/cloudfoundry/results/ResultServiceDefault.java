/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.results;

import java.util.List;

import org.apache.lucene.search.Query;

import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.search.IndexedBuildResultsSearcher;
import com.atlassian.bamboo.utils.collection.PartialList;
import com.atlassian.bonnie.LuceneUtils;

/**
 * @author David Ehringer
 */
public class ResultServiceDefault implements ResultService {

	private IndexedBuildResultsSearcher buildResultsSearcher;

	public ResultServiceDefault(IndexedBuildResultsSearcher buildResultsSearcher) {
		this.buildResultsSearcher = buildResultsSearcher;
	}

	@Override
	public List<ActivityGroup> recentActivity(int items) {
		Query query = LuceneUtils.buildSingleFieldSingleValueTermQuery(PushResult.CLOUDFOUNDRY_PUSH_TASK_KEY,
				PushResult.CLOUDFOUNDRY_PUSH_TASK_VALUE);
		PartialList<ResultsSummary> results = buildResultsSearcher.search(query, 10);
		return ActivityGroup.from(results.getList());
	}
}
