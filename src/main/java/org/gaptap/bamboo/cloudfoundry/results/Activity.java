/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.results;

import java.util.Date;
import java.util.List;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.google.common.collect.Lists;

/**
 * Homegrown activity stream solution until we can hook directly into activity streams in Bamboo.
 * 
 * @author David Ehringer
 */
public class Activity {

	private String planBuildKey;
	private String jobBuildKey;
	private String projectName;
	private String planName;
	private String jobName;
	private int buildNumber;
	private BuildState buildState;
	private String durationDescription;
	private String reasonSummary;
	private Date date;
	private List<PushResult> pushes = Lists.newArrayList();

	public String getPlanBuildKey() {
		return planBuildKey;
	}

	public void setPlanBuildKey(String planBuildKey) {
		this.planBuildKey = planBuildKey;
	}

	public String getJobBuildKey() {
		return jobBuildKey;
	}

	public void setJobBuildKey(String jobBuildKey) {
		this.jobBuildKey = jobBuildKey;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public int getBuildNumber() {
		return buildNumber;
	}

	public void setBuildNumber(int buildNumber) {
		this.buildNumber = buildNumber;
	}

	public BuildState getBuildState() {
		return buildState;
	}

	public void setBuildState(BuildState buildState) {
		this.buildState = buildState;
	}

	public List<PushResult> getPushes() {
		return pushes;
	}

	public void addPushes(List<PushResult> pushes) {
		this.pushes.addAll(pushes);
	}

	public String getDurationDescription() {
		return durationDescription;
	}

	public void setDurationDescription(String durationDescription) {
		this.durationDescription = durationDescription;
	}

	public String getReasonSummary() {
		return reasonSummary;
	}

	public void setReasonSummary(String reasonSummary) {
		this.reasonSummary = reasonSummary;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public static Activity from(ResultsSummary result){
		Activity activity = new Activity();
		activity.setDate(result.getBuildDate());
		activity.setBuildNumber(result.getBuildNumber());
		activity.setBuildState(result.getBuildState());
		activity.setJobBuildKey(result.getPlanResultKey().getKey());
		Plan plan = result.getPlan();
		activity.setJobName(plan.getBuildName());
		if (plan instanceof Job) {
			Job job = (Job) plan;
			Chain chain = job.getParent();
			activity.setPlanName(chain.getBuildName());
			activity.setPlanBuildKey(result.getPlanResultKey().getPlanKey().getKey());
			activity.setProjectName(chain.getProject().getName());
		} else {
			// TODO can we assume this can't happen?
		}

		activity.setDurationDescription(result.getDurationDescription());
		activity.setReasonSummary(result.getReasonSummary());

		// TODO why aren't our custom properties populated here? This is
		// always empty
		List<PushResult> pushes = PushResult.fromCustomBuildData(result.getCustomBuildData());
		activity.addPushes(pushes);
		return activity;
	}

}
