/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author David Ehringer
 */
public class ApplicationConfiguration {

    public static final int DEFAULT_DISK_QUOTA = 1024;
    public static final Integer PLATFORM_DEFAULT_TIMEOUT = null;
    public static final int DEFAULT_INSTANCE_COUNT = 1;
    public static final Integer DEFAULT_MEMORY = 1024;

    private String name;
    private Integer memory;
    /**
     * In MB.
     */
    private int diskQuota = DEFAULT_DISK_QUOTA;
    private int instances = DEFAULT_INSTANCE_COUNT;
    private String command;
    private String buildpackUrl;
    private String stack;
    private Map<String, String> environment = Maps.newHashMap();
    private List<String> urls = Lists.newArrayList();
    /**
     * Use the timeout attribute to give your application more time to start, up
     * to 180 seconds. This is equivalent to the -t option on the cf CLI. A
     * value of <code>null</code> can be specified to use the platform default
     * (which is generally 60 seconds).
     */
    private Integer startTimeout = PLATFORM_DEFAULT_TIMEOUT;

    /**
     * For binding to pre-existing services.
     */
    private List<String> serviceBindings = Lists.newArrayList();
    /**
     * Full definition of services. Can be used for either binding to
     * pre-existing services or creating new services .
     */
    private List<ServiceManifest> serviceManifests = Lists.newArrayList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMemory() {
        return memory;
    }

    /**
     * @param memory
     *            Memory in MB
     */
    public void setMemory(Integer memory) {
        if(memory == null){
            this.memory = DEFAULT_MEMORY;
        } else {
            this.memory = memory;
        }
    }

    public int getDiskQuota() {
        return diskQuota;
    }

    /**
     * @param diskQuota
     *            Disk quota in MB.
     */
    public void setDiskQuota(int diskQuota) {
        this.diskQuota = diskQuota;
    }

    public int getInstances() {
        return instances;
    }

    public void setInstances(Integer instances) {
        if(instances == null){
            this.instances = DEFAULT_INSTANCE_COUNT;
        }else {
            this.instances = instances;
        }
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Map<String, String> getEnvironment() {
        return environment;
    }

    public void addEnvironment(Map<String, String> environment) {
        this.environment.putAll(environment);
    }

    public List<String> getUrls() {
        return urls;
    }

    public void addUrls(List<String> urls) {
        this.urls.addAll(urls);
    }

    public Integer getStartTimeout() {
        return startTimeout;
    }

    public void setStartTimeout(Integer startTimeout) {
        this.startTimeout = startTimeout;
    }

    public List<String> getServiceBindings() {
        return serviceBindings;
    }

    public void addServiceBindings(List<String> serviceBindings) {
        this.serviceBindings.addAll(serviceBindings);
    }

    public List<ServiceManifest> getServiceManifests() {
        return serviceManifests;
    }

    public void addServiceManifests(List<ServiceManifest> serviceManifests) {
        this.serviceManifests.addAll(serviceManifests);
    }

    /**
     * A List of names of all the service bindings and service manifests. This
     * represents all the services that are to be bound to the application.
     */
    public List<String> getAllServiceNames() {
        List<String> serviceNames = Lists.newArrayList(serviceBindings);
        for (ServiceManifest serviceManifest : serviceManifests) {
            serviceNames.add(serviceManifest.getName());
        }
        return serviceNames;
    }

    public String getBuildpackUrl() {
        return buildpackUrl;
    }

    public void setBuildpackUrl(String buildpackUrl) {
        this.buildpackUrl = buildpackUrl;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public void setStack(String stack) {
        this.stack = stack;
    }

    public String getStack() {
        return stack;
    }
}
