/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cloudfoundry.client.lib.CloudFoundryException;
import org.cloudfoundry.client.lib.CloudFoundryOperations;
import org.cloudfoundry.client.lib.StartingInfo;
import org.cloudfoundry.client.lib.domain.ApplicationStats;
import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.cloudfoundry.client.lib.domain.CloudDomain;
import org.cloudfoundry.client.lib.domain.CloudInfo;
import org.cloudfoundry.client.lib.domain.CloudRoute;
import org.cloudfoundry.client.lib.domain.CloudService;
import org.cloudfoundry.client.lib.domain.CloudServiceOffering;
import org.cloudfoundry.client.lib.domain.Staging;
import org.cloudfoundry.maven.common.UiUtils;
import org.springframework.http.HttpStatus;

/**
 * @author David Ehringer
 * 
 */
public class CloudFoundryServiceVcap implements CloudFoundryService {

    private static final int STARTUP_MONITOR_INTERVAL = 5000;
    private final CloudFoundryOperations client;
    private final Logger logger;

    public CloudFoundryServiceVcap(CloudFoundryOperations client, Logger logger) {
        this.client = client;
        this.logger = logger;
    }

    @Override
    public CloudInfo info() {
        logger.info("Getting CloudInfo...");
        CloudInfo info = client.getCloudInfo();
        logger.info("Getting CloudInfo... OK");
        return info;
    }

    @Override
    public List<CloudApplication> apps() {
        logger.info("Getting applications...");
        List<CloudApplication> apps = client.getApplications();
        logger.info("Getting applications... OK");
        return apps;
    }

    @Override
    public CloudApplication app(String name) {
        logger.info(String.format("Getting application %s...", name));
        CloudApplication app = client.getApplication(name);
        logger.info(String.format("Getting application %s... OK", name));
        return app;
    }

    @Override
    public ApplicationStats appStats(String name) {
        logger.info(String.format("Getting stats for application %s...", name));
        ApplicationStats stats = client.getApplicationStats(name);
        logger.info(String.format("Getting stats for application %s... OK", name));
        return stats;
    }

    @Override
    public void startApp(String name) {
        startApp(name, 0);
    }

    @Override
    public boolean startApp(String name, int secondsToWait) throws CloudFoundryServiceException {
        assertApplicationExists(name);
        logger.info(String.format("Starting application %s...", name));
        client.startApplication(name);
        if (secondsToWait > 0) {
            logger.info(String.format("Waiting up to %d seconds for application to start", secondsToWait));
            return isSuccessfullyStarted(name, secondsToWait);
        }
        logger.info(String.format("Starting application %s... OK", name));
        return true;
    }

    private boolean isSuccessfullyStarted(String name, int secondsToWait) {
        boolean starting = true;
        long abortTime = System.currentTimeMillis() + (secondsToWait * 1000);
        while (starting && abortTime > System.currentTimeMillis()) {
            try {
                Thread.sleep(STARTUP_MONITOR_INTERVAL);
            } catch (InterruptedException e) {
                // nothing we can do
            }
            CloudApplication app = client.getApplication(name);
            if (app.getInstances() == app.getRunningInstances()) {
                starting = false;
            }
            logger.info("Checking startup status. Target number of instances: " + app.getInstances()
                    + ", Running instances: " + app.getRunningInstances());
        }
        CloudApplication app = client.getApplication(name);
        if (starting) {
            String message = "Out of " + app.getInstances() + " total instances, only " + app.getRunningInstances()
                    + " have started.";
            logger.error(message);
            return false;
        }
        logger.info("Application successfully started. Total instances: " + app.getInstances()
                + ", Running instances: " + app.getRunningInstances());
        return true;
    }

    @Override
    public void stopApp(String name) {
        if (applicationExists(name)) {
            logger.info(String.format("Stopping application %s...", name));
            client.stopApplication(name);
            logger.info(String.format("Stopping application %s... OK", name));
        } else {
            logger.warn(String.format("Application %s does not exist.", name));
        }
    }

    @Override
    public void restartApp(String name) {
        assertApplicationExists(name);
        logger.info(String.format("Retarting application %s...", name));
        client.restartApplication(name);
        logger.info(String.format("Retarting application %s... OK", name));
    }

    @Override
    public void deleteApp(String name) {
        if (applicationExists(name)) {
            logger.info(String.format("Deleting application %s...", name));
            client.deleteApplication(name);
            logger.info(String.format("Deleting application %s... OK", name));
        } else {
            logger.warn(String.format("Application %s does not exist.", name));
        }
    }

    @Override
    public void map(String appName, String uri) {
        CloudApplication app = client.getApplication(appName);
        List<String> uris = app.getUris();
        uris.add(uri);
        logger.info(String.format("Mapping URL %s to application %s...", uri, appName));
        client.updateApplicationUris(appName, uris);
        logger.info(String.format("Mapping URL %s to application %s... OK", uri, appName));
    }

    @Override
    public void unmap(String appName, String uri) {
        CloudApplication app = client.getApplication(appName);
        List<String> uris = app.getUris();
        uris.remove(uri);
        logger.info(String.format("Unmapping URL %s from application %s...", uri, appName));
        client.updateApplicationUris(appName, uris);
        logger.info(String.format("Unmapping URL %s from application %s... OK", uri, appName));
    }

    @Override
    public void renameApp(String appName, String newName, boolean failIfAppDoesNotExist) {
        logger.info(String.format("Renaming application named %s to %s...", appName, newName));
        if (!applicationExists(appName)) {
            if (failIfAppDoesNotExist) {
                String message = String.format("Unable to rename application named %s because it does not exist.",
                        appName);
                logger.error(message);
                throw new CloudFoundryServiceException(message);
            } else {
                logger.info(String
                        .format("Unable to rename application named %s because it does not exist. The operation will not be marked as failed because the 'Fail If Application Does Not Exist' option is disabled.",
                                appName));
            }
        } else {
            client.rename(appName, newName);
            logger.info(String.format("Renaming application named %s to %s... OK", appName, newName));
        }
    }

    @Override
    public List<CloudServiceOffering> serviceOfferings() {
        logger.info("Getting service offerings...");
        List<CloudServiceOffering> offerings = client.getServiceOfferings();
        logger.info("Getting service offerings... OK");
        return offerings;
    }

    @Override
    public List<CloudService> services() {
        logger.info("Getting services...");
        List<CloudService> services = client.getServices();
        logger.info("Getting services... OK");
        return services;
    }

    @Override
    public CloudService service(String name) {
        logger.info(String.format("Getting service %s...", name));
        CloudService service = client.getService(name);
        logger.info(String.format("Getting service %s... OK", name));
        return service;
    }

    @Override
    public void createService(String name, String provider, String label, String version, String plan,
            boolean failIfExists) {
        if (serviceExists(name)) {
            if (failIfExists) {
                String message = "Service with alias "
                        + name
                        + " already exists. The option to fail if a service already exists is enabled. The operation will fail.";
                logger.error(message);
                throw new CloudFoundryServiceException(message);
            }
            logger.warn("Service with alias " + name + " already exists. It will not be modified.");
            return;
        }

        CloudService cloudService = new CloudService();
        cloudService.setName(name);
        cloudService.setProvider(provider);
        cloudService.setLabel(label);
        cloudService.setVersion(version);
        cloudService.setPlan(plan);

        logger.info(String.format("Creating %s %s (%s, %s) service named %s...", provider, label, plan, version, name));
        client.createService(cloudService);
        logger.info(String.format("Creating %s %s (%s, %s) service named %s... OK", provider, label, plan, version,
                name));
    }

    @Override
    public void createUserProvidedService(String name, Map<String, Object> credentials, boolean ignoreIfExists) {
        if (serviceExists(name)) {
            if (ignoreIfExists) {
                logger.warn("Service with alias "
                        + name
                        + " already exists. The option to ignore if a service already exists is enabled. It will not be modified.");
                return;
            } else {
                String message = "Service with alias "
                        + name
                        + " already exists. The option to ignore if a service already exists is not enabled. The operation will fail.";
                logger.error(message);
                throw new CloudFoundryServiceException(message);
            }
        }

        CloudService service = new CloudService();
        service.setName(name);

        logger.info(String.format("Creating user provided service named %s...", name));
        client.createUserProvidedService(service, credentials);
        logger.info(String.format("Creating user provided service named %s... OK", name));
    }

    @Override
    public void deleteService(String name) {
        if (serviceExists(name)) {
            logger.info("Deleting service " + name + "...");
            client.deleteService(name);
            logger.info("Deleting service " + name + "... OK");
        } else {
            logger.warn("Service named " + name + " does not exist.");
        }
    }

    @Override
    public void bindService(String serviceName, String applicationName) {
        assertApplicationExists(applicationName);
        assertServiceExists(serviceName);
        if (serviceIsBoundTo(serviceName, applicationName)) {
            logger.warn(String.format("Service %s is already bound to %s. No modifications will be made.", serviceName,
                    applicationName));
        } else {
            logger.info(String.format("Binding service %s to application %s...", serviceName, applicationName));
            client.bindService(applicationName, serviceName);
            logger.info(String.format("Binding service %s to application %s... OK", serviceName, applicationName));
        }
    }

    /**
     * Binds all services that are not yet bound and unbinds services that
     * should no longer be bound. Assumes that the list of services has already
     * be validated so we know that they all exist.
     */
    private void syncServices(String applicationName, ApplicationConfiguration appConfig) {
        // client.updateApplicationServices(); basically does what this method
        // does but doesn't give the logging we want on why something is being
        // unbound.
        CloudApplication app = app(applicationName);
        bindMissingServices(applicationName, appConfig, app);
        unbindExtraServices(applicationName, appConfig, app);
    }

    private void bindMissingServices(String applicationName, ApplicationConfiguration appConfig, CloudApplication app) {
        for (String serviceName : appConfig.getAllServiceNames()) {
            if (!app.getServices().contains(serviceName)) {
                // the bindService() isn't used here because we don't need all
                // the additional checks we've already performed.
                logger.info(String.format("Binding service %s to application %s...", serviceName, applicationName));
                client.bindService(applicationName, serviceName);
                logger.info(String.format("Binding service %s to application %s... OK", serviceName, applicationName));
            }
        }
    }

    private void unbindExtraServices(String applicationName, ApplicationConfiguration appConfig, CloudApplication app) {
        for (String serviceName : app.getServices()) {
            if (!appConfig.getAllServiceNames().contains(serviceName)) {
                logger.info(String
                        .format("The service %s is currently bound to the application %s but should no longer be bound to it. The service will be unbound.",
                                serviceName, applicationName));
                // the unbindService() isn't used here because we don't need all
                // the additional checks we've already performed.
                logger.info(String.format("Unbinding service %s from application %s...", serviceName, applicationName));
                client.unbindService(applicationName, serviceName);
                logger.info(String.format("Unbinding service %s from application %s... OK", serviceName,
                        applicationName));
            }
        }
    }

    @Override
    public void unbindService(String serviceName, String applicationName) {
        assertApplicationExists(applicationName);
        assertServiceExists(serviceName);
        if (serviceIsBoundTo(serviceName, applicationName)) {
            logger.info(String.format("Unbinding service %s from application %s...", serviceName, applicationName));
            client.unbindService(applicationName, serviceName);
            logger.info(String.format("Unbinding service %s from application %s... OK", serviceName, applicationName));
        } else {
            logger.warn(String.format("Service %s is not bound to %s. No modifications will be made.", serviceName,
                    applicationName));
        }
    }

    private void assertServiceExists(String name) {
        if (!serviceExists(name)) {
            String message = String.format("Service %s does not exist.", name);
            logger.error(message);
            throw new CloudFoundryServiceException(message);
        }
    }

    private boolean serviceExists(String name) {
        logger.info(String.format("Checking for existance of service %s...", name));
        boolean exists = false;
        if(client.getService(name) != null){
            exists = true;
        }
        logger.info(String.format("Checking for existance of service %s... OK", name));
        return exists;
    }

    private void assertApplicationExists(String applicationName) {
        if (!applicationExists(applicationName)) {
            String message = String.format("Application %s does not exist.", applicationName);
            logger.error(message);
            throw new CloudFoundryServiceException(message);
        }
    }
    
    private boolean applicationExists(String applicationName) {
        logger.info(String.format( "Checking for existance of application %s...", applicationName));
        boolean found = false;
        try {
            if (client.getApplication(applicationName) != null) {
                // Checking for null to protect against changes in behavior of the getApplication call
                // because its behavior is inconsistent with getService
                found = true;
            }
        } catch (CloudFoundryException e) {
            if (!HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                throw new CloudFoundryServiceException(
                        String.format(
                                "Error while checking if application '%s' exists. Error message: '%s'. Description: '%s'",
                                applicationName, e.getMessage(),
                                e.getDescription()), e);
            }
        }
        logger.info(String.format("Checking for existance of application %s... OK",
                applicationName));
        return found;
    }

    /**
     * Assumes application exists
     */
    private boolean serviceIsBoundTo(String serviceName, String applicationName) {
        logger.info(String.format("Checking if service %s is bound to application %s...", serviceName, applicationName));
        CloudApplication application = client.getApplication(applicationName);
        for (String service : application.getServices()) {
            if (service.equals(serviceName)) {
                logger.info(String.format("Checking if service %s is bound to application %s... OK", serviceName,
                        applicationName));
                return true;
            }
        }
        logger.info(String.format(
                "Checking if service %s is bound to application %s... OK (service is NOT bound to application)",
                serviceName, applicationName));
        return false;
    }

    @Override
    public void push(ApplicationConfiguration appConfig, File application, boolean start) {
        push(appConfig.getName(), appConfig, application, start, 0);
    }

    @Override
    public boolean push(ApplicationConfiguration appConfig, File application, int secondsToWait)
            throws CloudFoundryServiceException {
        return push(appConfig.getName(), appConfig, application, true, secondsToWait);
    }

    private boolean push(String name, ApplicationConfiguration appConfig, File application, boolean start,
            int secondsToWait) throws CloudFoundryServiceException {
        validateServicesExistForBinding(appConfig);
        createNewServicesIfNeeded(appConfig);

        if (!applicationExists(name)) {
            createApplication(name, appConfig);
        } else {
            logger.info(String.format("Application %s already exists. Syncing changes.", name));
            logApplicationState(name);
            logger.info("Stopping application");
            stopApp(name);
        }

        uploadApplication(name, application);

        syncConfiguration(name, appConfig);
        syncServices(name, appConfig);
        if (start) {
            boolean successfullyStarted = startApp(name, secondsToWait);
            logApplicationState(name);
            return successfullyStarted;
        }
        logger.info("Application will not be started.");
        return true;
    }

    private void validateServicesExistForBinding(ApplicationConfiguration appConfig) {
        if (!appConfig.getServiceBindings().isEmpty()) {
            logger.info("Checking for existance of services to be bound to application.");
        }

        List<String> invalidServices = new ArrayList<String>();
        for (String service : appConfig.getServiceBindings()) {
            if (!serviceExists(service)) {
                invalidServices.add(service);
            }
        }
        if (invalidServices.size() > 0) {
            StringBuilder message = new StringBuilder();
            message.append("The following services that you've attempt to bind to the application do not exist: ");
            Iterator<String> invalidIter = invalidServices.iterator();
            while (invalidIter.hasNext()) {
                message.append(invalidIter.next());
                if (invalidIter.hasNext()) {
                    message.append(",");
                }
            }
            logger.error(message.toString());
            throw new CloudFoundryServiceException(message.toString());
        }
    }

    private void logApplicationState(String name) {
        logger.info("Current state of application:");
        CloudApplication app = app(name);
        ApplicationStats appStats = appStats(name);

        String[] output = UiUtils.renderCloudApplicationDataAsTable(app, appStats).split("\\n");
        for (String line : output) {
            logger.info(line);
        }

        output = UiUtils.renderEnvVarDataAsTable(app).split("\\n");
        for (String line : output) {
            logger.info(line);
        }
    }

    private void createNewServicesIfNeeded(ApplicationConfiguration appConfig) {
        for (ServiceManifest serviceManifest : appConfig.getServiceManifests()) {
            if (!serviceExists(serviceManifest.getName())) {
                createService(serviceManifest.getName(), serviceManifest.getProvider(), serviceManifest.getLabel(),
                        serviceManifest.getVersion(), serviceManifest.getPlan(), false);
            } else {
                assertVendorMatchesAlreadyProvisionedService(serviceManifest);
            }
        }
    }

    private void assertVendorMatchesAlreadyProvisionedService(ServiceManifest serviceManifest) {
        CloudService service = service(serviceManifest.getName());
        String provider = service.getProvider();
        if (!serviceManifest.getProvider().equalsIgnoreCase(provider)) {
            // TODO check for label as well?
            String message = "Application requires " + serviceManifest + " but a " + provider
                    + " (a different provider) service already exists with the same name.";
            logger.error(message);
            throw new CloudFoundryServiceException(message);
        }
    }

    /**
     * Assumes application does not exist
     */
    private void createApplication(String appName, ApplicationConfiguration appConfig) {
        logger.info("Application does not exist. Creating new application: " + appName);
        Staging staging = buildStaging(appConfig);
        // We could pass in service names to createApplication() and it would do
        // some of what we do elsewhere but we don't get the logging to the
        // build log that we want.
        logger.info(String.format("Creating application %s...", appName));
        client.createApplication(appName, staging, appConfig.getDiskQuota(), appConfig.getMemory(),
                appConfig.getUrls(), new ArrayList<String>());
        logger.info(String.format("Creating application %s... OK", appName));
    }

    private Staging buildStaging(ApplicationConfiguration appConfig) {
        return new Staging(appConfig.getCommand(), appConfig.getBuildpackUrl(), appConfig.getStack(), appConfig.getStartTimeout());
    }

    private void uploadApplication(String appName, File applicationFile) {
        logger.info("Preparing upload of application " + appName);
        logger.info("Application File: " + applicationFile);
        try {
            logger.info(String.format("Uploading application %s...", appName));
            client.uploadApplication(appName, applicationFile, new UploadStatusCallbackLogger(logger));
        } catch (IOException e) {
            String message = "Exception occured when uploading application: " + e.getMessage();
            logger.error(message);
            throw new CloudFoundryServiceException(message, e);
        }
        logger.info(String.format("Uploading application %s... OK", appName));
    }

    private void syncConfiguration(String appName, ApplicationConfiguration appConfig) {
        logger.info("Synchronizing application configuration.");

        Staging staging = buildStaging(appConfig);
        logger.info("Updating application staging...");
        client.updateApplicationStaging(appName, staging);
        logger.info("Updating application staging... OK");

        logger.info("Updating application URLs...");
        client.updateApplicationUris(appName, appConfig.getUrls());
        logger.info("Updating application URLs... OK");

        logger.info("Updating application environment...");
        client.updateApplicationEnv(appName, appConfig.getEnvironment());
        logger.info("Updating application environment... OK");

        logger.info("Updating application instances...");
        client.updateApplicationInstances(appName, appConfig.getInstances());
        logger.info("Updating application instances... OK");

        logger.info("Updating application memory...");
        client.updateApplicationMemory(appName, appConfig.getMemory());
        logger.info("Updating application memory... OK");

        logger.info("Updating application disk quota...");
        client.updateApplicationDiskQuota(appName, appConfig.getDiskQuota());
        logger.info("Updating application disk quota... OK");

        logger.info("Synchronization of application configuration complete.");
    }

    @Override
    public void addDomain(String domain) {
        logger.info(String.format("Adding domain %s", domain));
        client.addDomain(domain);
        logger.info(String.format("Adding domain %s... OK", domain));
    }

    @Override
    public void deleteDomain(String domain) {
        if (domainExists(domain)) {
            logger.info(String.format("Deleting domain %s", domain));
            client.deleteDomain(domain);
            logger.info(String.format("Deleting domain %s... OK", domain));
        } else {
            logger.info(String.format("Domain '%s' doesn't exist. No action will be taken.", domain));
        }
    }

    @Override
    public void addRoute(String domain, String host) {
        logger.info(String.format("Adding route - host: %s, domain: %s", host, domain));
        client.addRoute(host, domain);
        logger.info(String.format("Adding route - host: %s, domain: %s... OK", host, domain));
    }

    @Override
    public void deleteRoute(String domain, String host) {
        if (domainExists(domain)) {
            if (routeExists(domain, host)) {
                logger.info(String.format("Deleting route - host: %s, domain: %s", host, domain));
                client.deleteRoute(host, domain);
                logger.info(String.format("Deleting route - host: %s, domain: %s... OK", host, domain));
            } else {
                logger.info(String
                        .format("Host '%s' not found for domain '%s'. No action will be taken.", host, domain));
            }
        } else {
            logger.info(String.format("Domain '%s' doesn't exist. No action will be taken.", domain));
        }

    }

    private boolean domainExists(String domain) {
        boolean exists = false;
        for (CloudDomain cloudDomain : client.getDomains()) {
            if (cloudDomain.getName().equalsIgnoreCase(domain)) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    private boolean routeExists(String domain, String host) {
        boolean exists = false;
        for (CloudRoute route : client.getRoutes(domain)) {
            if (route.getHost().equalsIgnoreCase(host)) {
                exists = true;
                break;
            }
        }
        return exists;
    }
}
