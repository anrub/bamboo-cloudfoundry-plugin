/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.apache.commons.lang.Validate;

/**
 * The full definition of a service in Cloud Foundry. If you are simply looking to reference 
 * bindings to pre-existing services, use {@link ApplicationConfiguration#getServiceBindings()}.
 * 
 * @author David Ehringer
 */
public class ServiceManifest {

    // TODO do we need this any longer or can CloudService do the job?

    private String name;
    private String label;
    private String provider;
    private String plan;
    private String version;

    // The Java Client only requires name, label, and plan

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Validate.notNull(name, "Service name cannot be null");
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        Validate.notNull(label, "Service label cannot be null");
        this.label = label;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        Validate.notNull(plan, "Service plan cannot be null");
        this.plan = plan;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ServiceManifest [name=" + name + ", label=" + label + ", provider=" + provider + ", plan=" + plan
                + ", version=" + version + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((plan == null) ? 0 : plan.hashCode());
        result = prime * result + ((provider == null) ? 0 : provider.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ServiceManifest other = (ServiceManifest) obj;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (plan == null) {
            if (other.plan != null)
                return false;
        } else if (!plan.equals(other.plan))
            return false;
        if (provider == null) {
            if (other.provider != null)
                return false;
        } else if (!provider.equals(other.provider))
            return false;
        if (version == null) {
            if (other.version != null)
                return false;
        } else if (!version.equals(other.version))
            return false;
        return true;
    }

}
