/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import java.net.MalformedURLException;
import java.net.URL;

import org.cloudfoundry.client.lib.CloudCredentials;
import org.cloudfoundry.client.lib.CloudFoundryClient;
import org.cloudfoundry.client.lib.CloudFoundryException;
import org.cloudfoundry.client.lib.CloudFoundryOperations;
import org.cloudfoundry.client.lib.HttpProxyConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import com.atlassian.bamboo.security.EncryptionService;

/**
 * @author David Ehringer
 * 
 */
public class CloudFoundryServiceFactoryVcap implements CloudFoundryServiceFactory {

    private final EncryptionService encryptionService;

    public CloudFoundryServiceFactoryVcap(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @Override
    public CloudFoundryOperations getClient(ConnectionParameters connectionParameters, String org, String space,
            Logger logger) throws CloudFoundryServiceException {
        CloudCredentials credentials = getCredentials(connectionParameters);
        HttpProxyConfiguration proxy = getProxy(connectionParameters);
        CloudFoundryClient client = createClient(credentials, proxy, connectionParameters, org, space, logger);
        loginToCloudFoundry(client, logger);
        return client;
    }

    private CloudFoundryClient createClient(CloudCredentials credentials, HttpProxyConfiguration proxy, ConnectionParameters connectionParameters,
            String org, String space, Logger logger) {
        // TODO can this be cleaner
        URL target = getUrl(connectionParameters);
        try {
            if (proxy == null) {
                if (org == null || space == null) {
                    return new CloudFoundryClient(credentials, target, connectionParameters.isTrustSelfSignedCerts());
                } else {
                    return new CloudFoundryClient(credentials, target, org, space, connectionParameters.isTrustSelfSignedCerts());
                }
            } else {
                if (org == null || space == null) {
                    return new CloudFoundryClient(credentials, target, proxy, connectionParameters.isTrustSelfSignedCerts());
                } else {
                    return new CloudFoundryClient(credentials, target, org, space, proxy, connectionParameters.isTrustSelfSignedCerts());
                }
            }
            // TODO this needs to look like the login below
        } catch (HttpStatusCodeException e) {
            if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                String message = "The target host exists but it does not appear to be a valid Cloud Foundry target url.";
                logger.error(message);
                throw new InvalidTargetUrlException(message, e);
            }
            String message = "Unable to connect to target due to an unknown error: " + e.getMessage();
            logger.error(message);
            throw new InvalidTargetUrlException(message);
        } catch (RestClientException e) {
            String message = "Unable to connect to target due to an unknown error: " + e.getMessage();
            logger.error(message);
            throw new InvalidTargetUrlException(message);
        }
    }

    private URL getUrl(ConnectionParameters connectionParameters) {
        URL target = null;
        try {
            target = new URL(connectionParameters.getTargetUrl());
        } catch (MalformedURLException e) {
            throw new InvalidTargetUrlException(e.getMessage(), e);
        }
        return target;
    }

    private CloudCredentials getCredentials(ConnectionParameters connectionParameters) {
        String username = connectionParameters.getUsername();
        String password = connectionParameters.getPassword();
        return new CloudCredentials(username, encryptionService.decrypt(password));
    }

    private HttpProxyConfiguration getProxy(ConnectionParameters connectionParameters) {
        String proxyHost = connectionParameters.getProxyHost();
        if (proxyHost != null) {
            int proxyPort = connectionParameters.getProxyPort();
            return new HttpProxyConfiguration(proxyHost, proxyPort);
        }
        return null;
    }

    protected void loginToCloudFoundry(CloudFoundryOperations client, Logger logger) {
        try {
            client.login();
        } catch (CloudFoundryException e) {
            String message = null;
            if (HttpStatus.FORBIDDEN.equals(e.getStatusCode())) {
                message = "Login failed";
                logger.error(message);
                throw new CloudAuthenticationException(message, e);
            } else if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                message = "The target host exists but it does not appear to be a valid Cloud Foundry target url.";
                logger.error(message);
                throw new InvalidTargetUrlException(message, e);
            }
            message = "Unknown exception occured when attepting to connect to target.";
            logger.error(message);
            throw new CloudFoundryServiceException(message, e);
        }
    }

    public CloudFoundryService getCloudFoundryService(ConnectionParameters connectionParameters, Logger logger) {
        return new CloudFoundryServiceVcap(getClient(connectionParameters, null, null, logger), logger);
    }

    public CloudFoundryService getCloudFoundryService(ConnectionParameters connectionParameters, String org,
            String space, Logger logger) {
        return new CloudFoundryServiceVcap(getClient(connectionParameters, org, space, logger), logger);
    }
}
