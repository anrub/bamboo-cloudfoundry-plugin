/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import java.text.NumberFormat;
import java.util.Set;

import org.cloudfoundry.client.lib.UploadStatusCallback;

/**
 * @author David Ehringer
 */
public class UploadStatusCallbackLogger implements UploadStatusCallback {

    private final Logger logger;

    public UploadStatusCallbackLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void onProcessMatchedResources(int length) {
        NumberFormat formatter = NumberFormat.getInstance();
        logger.info("The size (pre-compression) of the resources to upload is " + formatter.format(length) + " bytes.");
    }

    @Override
    public void onMatchedFileNames(Set<String> matchedFileNames) {
        if (matchedFileNames.isEmpty()) {
            logger.info("All files will be uploaded (none already exist remotely).");
        } else {
            logger.info("The following files already exist remotely and will not be uploaded: ");
            for (String name : matchedFileNames) {
                logger.info(name);
            }
        }
    }

    @Override
    public void onCheckResources() {
    }

    @Override
    public boolean onProgress(String status) {
        logger.info("Upload status: " + status);
        return false;
    }
}
