/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.cloudfoundry.client.lib.domain.ApplicationStats;
import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.cloudfoundry.client.lib.domain.CloudInfo;
import org.cloudfoundry.client.lib.domain.CloudService;
import org.cloudfoundry.client.lib.domain.CloudServiceOffering;

/**
 * A set of Cloud Foundry functions applicable to a build server and delivery
 * pipelines. Analogous to the CF command line.
 * 
 * @author David Ehringer
 * 
 */
public interface CloudFoundryService {

    CloudInfo info();

    List<CloudApplication> apps();

    CloudApplication app(String name);

    ApplicationStats appStats(String name);

    void push(ApplicationConfiguration applicationConfiguration, File application, boolean start);

    /**
     * Push an application and start it.
     * 
     * @param applicationConfiguration
     * @param application
     * @param secondsToWait
     *            seconds to wait for all instances of the app to start before
     *            returning a status
     * @return true if all instances of the app successfully start within
     *         <code>secondsToWait</code>, and false.
     * @throws CloudFoundryServiceException
     */
    boolean push(ApplicationConfiguration applicationConfiguration, File application, int secondsToWait)
            throws CloudFoundryServiceException;

    /**
     * Trigger an application to start.
     * 
     * @param name
     */
    void startApp(String name);

    /**
     * Trigger an application to start and monitor it until all instances of it
     * start. If all instances of the application have not started after
     * <code>secondsToWait</code> seconds, <code>false</code> is returned.
     * 
     * @param name
     * @param secondsToWait
     *            0 or a negative number means do not wait
     */
    boolean startApp(String name, int secondsToWait);

    void stopApp(String name);

    void restartApp(String name);

    void deleteApp(String name);

    void map(String appName, String uri);

    void unmap(String appName, String uri);

    /**
     * 
     * @param appName
     * @param newName
     * @param failIfAppDoesNotExist
     *            throw a CloudFoundryServiceException if an application named
     *            <code>appName</code> does not exist
     * @throws CloudFoundryServiceException
     *             if <code>failIfAppDoesNotExist</code> is true and an
     *             application named <code>appName</code> does not exist
     */
    void renameApp(String appName, String newName, boolean failIfAppDoesNotExist);

    List<CloudServiceOffering> serviceOfferings();

    List<CloudService> services();

    CloudService service(String name);

    /**
     * 
     * @param name
     *            your name for the service
     * @param provider
     *            the service provider name
     * @param label
     *            the provider's name for the service
     * @param version
     *            the version of the service (e.g. n/a)
     * @param plan
     * @param failIfExists
     *            throw a CloudFoundryServiceException if a service named
     *            <code>name</code> already exists
     * @throws CloudFoundryServiceException
     *             if <code>failIfExsits</code> is true and the service exists
     */
    void createService(String name, String provider, String label, String version, String plan, boolean failIfExists)
            throws CloudFoundryServiceException;

    /**
     * 
     * @param name
     *            your name for the service
     * @param credentials
     *            A map representing the JSON v
     * @param ignoreIfExists
     *            if the service exists and ignoreIfExists is set to true, take
     *            not action. Otherwise when the service exists, the task will
     *            fail
     * @throws CloudFoundryServiceException
     *             if <code>ignoreIfExists</code> is false and the service
     *             exists
     */
    void createUserProvidedService(String name, Map<String, Object> credentials, boolean ignoreIfExists);

    void deleteService(String name);

    void bindService(String serviceName, String applicationName);

    void unbindService(String serviceName, String applicationName);

    void addDomain(String domain);

    void deleteDomain(String domain);

    void addRoute(String domain, String host);

    void deleteRoute(String domain, String host);
}
