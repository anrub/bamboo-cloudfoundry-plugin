/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.dashboard;

import java.util.Collection;
import java.util.List;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.gaptap.bamboo.cloudfoundry.results.ActivityGroup;
import org.gaptap.bamboo.cloudfoundry.results.ResultService;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
@SuppressWarnings("serial")
public class ViewDashboardAction extends BambooActionSupport {

	private List<ActivityGroup> activityGroups = Lists.newArrayList();

	private final CloudFoundryAdminService adminService;
	private ResultService resultService;

	public ViewDashboardAction(CloudFoundryAdminService adminService, ResultService resultService) {
		this.adminService = adminService;
		this.resultService = resultService;
	}

	@Override
	public String doDefault() throws Exception {
		activityGroups.addAll(resultService.recentActivity(10));
		return SUCCESS;
	}

	public Collection<Target> getTargets() {
		return adminService.allTargets();
	}

	public List<ActivityGroup> getActivityGroups() {
		return activityGroups;
	}

	public void setActivityTroups(List<ActivityGroup> activityGroups) {
		this.activityGroups = activityGroups;
	}

}
