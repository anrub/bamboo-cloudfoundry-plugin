/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.cloudfoundry.client.lib.domain.InstanceStats.Usage;

/**
 * @author David Ehringer
 * 
 */
@XmlRootElement(name = "usage")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstanceUsageRepresentation {

	@XmlAttribute
	private double cpu;
	@XmlAttribute
	private int disk;
	@XmlAttribute
	private double mem;
	@XmlAttribute
	private Date time;

	public double getCpu() {
		return cpu;
	}

	public void setCpu(double cpu) {
		this.cpu = cpu;
	}

	public int getDisk() {
		return disk;
	}

	public void setDisk(int disk) {
		this.disk = disk;
	}

	public double getMem() {
		return mem;
	}

	public void setMem(double mem) {
		this.mem = mem;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public static InstanceUsageRepresentation from(Usage usage) {
		InstanceUsageRepresentation restUsage = new InstanceUsageRepresentation();
		restUsage.setCpu(usage.getCpu());
		restUsage.setDisk(usage.getDisk());
		restUsage.setMem(usage.getMem());
		restUsage.setTime(usage.getTime());
		return restUsage;
	}
}
