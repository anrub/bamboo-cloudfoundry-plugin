/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.cloudfoundry.client.lib.domain.ApplicationStats;
import org.cloudfoundry.client.lib.domain.CloudApplication;

/**
 * @author David Ehringer
 * 
 */
@XmlRootElement(name = "applicationStats")
@XmlAccessorType(XmlAccessType.FIELD)
public class ApplicationStatsRepresentation {

	@XmlElement
	private AppRepresentation app;

	@XmlElement
	private List<InstanceStatsRepresentation> instances;

	public AppRepresentation getApp() {
		return app;
	}

	public void setApp(AppRepresentation app) {
		this.app = app;
	}

	public List<InstanceStatsRepresentation> getInstances() {
		return instances;
	}

	public void setInstances(List<InstanceStatsRepresentation> instances) {
		this.instances = instances;
	}

	public static ApplicationStatsRepresentation from(CloudApplication cloudApp, ApplicationStats stats) {
		ApplicationStatsRepresentation restStats = new ApplicationStatsRepresentation();
		restStats.setApp(AppRepresentation.from(cloudApp));
		restStats.setInstances(InstanceStatsRepresentation.from(stats.getRecords()));
		return restStats;
	}
}
