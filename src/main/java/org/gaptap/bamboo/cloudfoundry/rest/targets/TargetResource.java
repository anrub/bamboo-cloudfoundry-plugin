/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.cloudfoundry.client.lib.CloudFoundryException;
import org.cloudfoundry.client.lib.domain.ApplicationStats;
import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.cloudfoundry.client.lib.domain.CloudInfo;
import org.cloudfoundry.client.lib.domain.CloudService;
import org.cloudfoundry.client.lib.domain.CloudServiceOffering;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Credentials;
import org.gaptap.bamboo.cloudfoundry.admin.Proxy;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.gaptap.bamboo.cloudfoundry.client.CloudAuthenticationException;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceException;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.gaptap.bamboo.cloudfoundry.client.InvalidTargetUrlException;
import org.gaptap.bamboo.cloudfoundry.client.Log4jLogger;
import org.gaptap.bamboo.cloudfoundry.rest.ErrorStatusRepresentation;

/**
 * @author David Ehringer
 * 
 */
@Path("/targets")
@Produces({ MediaType.APPLICATION_JSON })
public class TargetResource {

	private static final Logger LOG = Logger.getLogger(TargetResource.class);

	private final CloudFoundryAdminService adminService;
	private final CloudFoundryServiceFactory cloudFoundryClientFactory;

	public TargetResource(CloudFoundryAdminService adminService, CloudFoundryServiceFactory cloudFoundryClientFactory) {
		this.adminService = adminService;
		this.cloudFoundryClientFactory = cloudFoundryClientFactory;
	}

	// TODO AbstractCloudFoundryTask has this same logic (implemented differently)
	// We need to combine it.
	private CloudFoundryService createClient(int targetId) {
		Target target = adminService.getTarget(targetId);
		ConnectionParameters connectionParameters = new ConnectionParameters();
		connectionParameters.setTargetUrl(target.getUrl());
		connectionParameters.setTrustSelfSignedCerts(target.isTrustSelfSignedCerts());

		Credentials credentials = target.getCredentials();
		connectionParameters.setUsername(credentials.getUsername());
		connectionParameters.setPassword(credentials.getPassword());

		Proxy proxy = target.getProxy();
		if (proxy != null) {
			connectionParameters.setProxyHost(proxy.getHost());
			connectionParameters.setProxyPort(proxy.getPort());
		}

		CloudFoundryService client = null;
		try {
			client = cloudFoundryClientFactory.getCloudFoundryService(connectionParameters, new Log4jLogger());
		} catch (InvalidTargetUrlException e) {
			throw e;
		} catch (CloudAuthenticationException e) {
			throw e;
		} catch (CloudFoundryServiceException e) {
			throw e;
		}
		return client;
	}

	@GET
	@Path("/")
	public Response getTargets() {
		List<Target> targets = adminService.allTargets();
		List<TargetRepresentation> restTargets = TargetRepresentation.from(targets);
		return Response.ok(restTargets).build();
	}

	@GET
	@Path("/{targetId}")
	public Response getTargetInfo(@PathParam("targetId") String targetIdString) {

		// TODO validate string
		int targetId = Integer.parseInt(targetIdString);
		CloudFoundryService client = createClient(targetId);
		CloudInfo info = client.info();

		CloudInfoRepresentation restInfo = CloudInfoRepresentation.from(info);

		Target target = adminService.getTarget(targetId);
		restInfo.setTargetName(target.getName());

		return Response.ok(restInfo).build();
	}

	@GET
	@Path("/{targetId}/serviceOfferings")
	public Response getServiceOfferings(@PathParam("targetId") String targetId) {
		LOG.info("Target ID: " + targetId);

		// TODO validate string
		CloudFoundryService client = createClient(Integer.parseInt(targetId));
		List<CloudServiceOffering> serviceConfigurations = client.serviceOfferings();

		List<ServiceOfferingRepresentation> restConfigurations = ServiceOfferingRepresentation.from(serviceConfigurations);

		return Response.ok(restConfigurations).build();
	}

	@GET
	@Path("/{targetId}/services/{serviceName}")
	public Response getService(@PathParam("targetId") String targetId, @PathParam("serviceName") String serviceName) {
		LOG.info("Target ID: " + targetId);

		// TODO validate string
		CloudFoundryService client = createClient(Integer.parseInt(targetId));

		CloudService service = null;
		try {
			service = client.service(serviceName);
		} catch (CloudFoundryException e) {
			return Response.status(Status.NOT_FOUND).entity(new ErrorStatusRepresentation("Service does not exist.")).build();
		}
		ServiceRepresentation restService = ServiceRepresentation.from(service);

		return Response.ok(restService).build();
	}

	@GET
	@Path("/{targetId}/apps")
	public Response getApps(@PathParam("targetId") String targetId) {

		// TODO validate string
		CloudFoundryService client = createClient(Integer.parseInt(targetId));

		List<CloudApplication> apps = client.apps();
		List<AppRepresentation> restApps = AppRepresentation.from(apps);

		return Response.ok(restApps).build();
	}

	@GET
	@Path("/{targetId}/apps/{applicationName}/stats")
	public Response getAppStats(@PathParam("targetId") String targetId,
			@PathParam("applicationName") String applicationName) {

		// TODO validate string
		CloudFoundryService client = createClient(Integer.parseInt(targetId));

		ApplicationStats stats = client.appStats(applicationName);
		CloudApplication app = client.app(applicationName);
		ApplicationStatsRepresentation restStats = ApplicationStatsRepresentation.from(app, stats);

		return Response.ok(restStats).build();
	}
}
