/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.activity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.gaptap.bamboo.cloudfoundry.results.Activity;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
@XmlRootElement(name = "activity")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityRepresentation {

	@XmlAttribute
	private String planBuildKey;
	@XmlAttribute
	private String jobBuildKey;
	@XmlAttribute
	private String projectName;
	@XmlAttribute
	private String planName;
	@XmlAttribute
	private String jobName;
	@XmlAttribute
	private int buildNumber;
	@XmlAttribute
	private String buildState;
	@XmlAttribute
	private String durationDescription;
	@XmlAttribute
	private String reasonSummary;

	public String getPlanBuildKey() {
		return planBuildKey;
	}

	public void setPlanBuildKey(String planBuildKey) {
		this.planBuildKey = planBuildKey;
	}

	public String getJobBuildKey() {
		return jobBuildKey;
	}

	public void setJobBuildKey(String jobBuildKey) {
		this.jobBuildKey = jobBuildKey;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public int getBuildNumber() {
		return buildNumber;
	}

	public void setBuildNumber(int buildNumber) {
		this.buildNumber = buildNumber;
	}

	public String getBuildState() {
		return buildState;
	}

	public void setBuildState(String buildState) {
		this.buildState = buildState;
	}

	public String getDurationDescription() {
		return durationDescription;
	}

	public void setDurationDescription(String durationDescription) {
		this.durationDescription = durationDescription;
	}

	public String getReasonSummary() {
		return reasonSummary;
	}

	public void setReasonSummary(String reasonSummary) {
		this.reasonSummary = reasonSummary;
	}

	public static ActivityRepresentation from(Activity activity) {
		ActivityRepresentation rep = new ActivityRepresentation();
		rep.buildNumber = activity.getBuildNumber();
		rep.buildState = activity.getBuildState().name();
		rep.durationDescription = activity.getDurationDescription();
		rep.jobBuildKey = activity.getJobBuildKey();
		rep.jobName = activity.getJobName();
		rep.planBuildKey = activity.getPlanBuildKey();
		rep.planName = activity.getPlanName();
		rep.projectName = activity.getProjectName();
		rep.reasonSummary = activity.getReasonSummary();
		return rep;
	}

	public static List<ActivityRepresentation> from(List<Activity> activities) {
		List<ActivityRepresentation> reps = Lists.newArrayList();
		for (Activity activity : activities) {
			reps.add(from(activity));
		}
		return reps;
	}
}
