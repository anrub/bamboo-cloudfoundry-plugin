/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.cloudfoundry.client.lib.domain.InstanceStats;

import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 * 
 */
@XmlRootElement(name = "instanceStats")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstanceStatsRepresentation {

	@XmlAttribute
	private int cores;
	@XmlAttribute
	private long diskQuota;
	@XmlAttribute
	private int fdsQuota;
	@XmlAttribute
	private String host;
	@XmlAttribute
	private String id;
	@XmlAttribute
	private long memQuota;
	@XmlAttribute
	private String name;
	@XmlAttribute
	private int port;
	@XmlAttribute
	private String state;
	@XmlAttribute
	private double uptime;

	@XmlElement
	private List<String> uris;

	@XmlElement
	private InstanceUsageRepresentation usage;

	public int getCores() {
		return cores;
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public long getDiskQuota() {
		return diskQuota;
	}

	public void setDiskQuota(long diskQuota) {
		this.diskQuota = diskQuota;
	}

	public int getFdsQuota() {
		return fdsQuota;
	}

	public void setFdsQuota(int fdsQuota) {
		this.fdsQuota = fdsQuota;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getMemQuota() {
		return memQuota;
	}

	public void setMemQuota(long memQuota) {
		this.memQuota = memQuota;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getUptime() {
		return uptime;
	}

	public void setUptime(double uptime) {
		this.uptime = uptime;
	}

	public List<String> getUris() {
		return uris;
	}

	public void setUris(List<String> uris) {
		this.uris = uris;
	}

	public InstanceUsageRepresentation getUsage() {
		return usage;
	}

	public void setUsage(InstanceUsageRepresentation usage) {
		this.usage = usage;
	}

	public static InstanceStatsRepresentation from(InstanceStats stats) {
		InstanceStatsRepresentation restStats = new InstanceStatsRepresentation();
		restStats.setCores(stats.getCores());
		restStats.setDiskQuota(stats.getDiskQuota());
		restStats.setFdsQuota(stats.getFdsQuota());
		restStats.setHost(stats.getHost());
		restStats.setId(stats.getId());
		restStats.setMemQuota(stats.getMemQuota());
		restStats.setName(stats.getName());
		restStats.setPort(stats.getPort());
		restStats.setState(stats.getState().toString());
		restStats.setUptime(stats.getUptime());
		restStats.setUris(stats.getUris());
		restStats.setUsage(InstanceUsageRepresentation.from(stats.getUsage()));
		return restStats;
	}

	public static List<InstanceStatsRepresentation> from(List<InstanceStats> stats) {
		List<InstanceStatsRepresentation> restStats = Lists.newArrayList();
		for (InstanceStats stat : stats) {
			restStats.add(from(stat));
		}
		return restStats;
	}
}
