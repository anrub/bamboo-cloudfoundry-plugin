/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.cloudfoundry.client.lib.domain.CloudInfo;

/**
 * @author David Ehringer
 * 
 */
@XmlRootElement(name = "info")
@XmlAccessorType(XmlAccessType.FIELD)
public class CloudInfoRepresentation {

	// private Limits limits;
	// private Usage usage;
	@XmlAttribute
	private String targetName;
	@XmlAttribute
	private String name;
	@XmlAttribute
	private String support;
	@XmlAttribute
	private String build;
	@XmlAttribute
	private String version;
	@XmlAttribute
	private String user;
	@XmlAttribute
	private String description;
	@XmlAttribute
	private String authorizationEndpoint;
	@XmlAttribute
	private boolean allowDebug;

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSupport() {
		return support;
	}

	public void setSupport(String support) {
		this.support = support;
	}

	public String getBuild() {
		return build;
	}

	public void setBuild(String build) {
		this.build = build;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthorizationEndpoint() {
		return authorizationEndpoint;
	}

	public void setAuthorizationEndpoint(String authorizationEndpoint) {
		this.authorizationEndpoint = authorizationEndpoint;
	}

	public boolean isAllowDebug() {
		return allowDebug;
	}

	public void setAllowDebug(boolean allowDebug) {
		this.allowDebug = allowDebug;
	}

	public static CloudInfoRepresentation from(CloudInfo info) {
		CloudInfoRepresentation restInfo = new CloudInfoRepresentation();
		restInfo.setAllowDebug(info.getAllowDebug());
		restInfo.setAuthorizationEndpoint(info.getAuthorizationEndpoint());
		restInfo.setBuild(info.getBuild());
		restInfo.setDescription(info.getDescription());
		restInfo.setName(info.getName());
		restInfo.setSupport(info.getSupport());
		restInfo.setUser(info.getUser());
		restInfo.setVersion(info.getVersion());
		return restInfo;
	}

}
