/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.activity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.gaptap.bamboo.cloudfoundry.results.ActivityGroup;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
@XmlRootElement(name = "activityGroup")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityGroupRepresentation {

	@XmlAttribute
	private String dateDescription;

	@XmlElement
	private List<ActivityRepresentation> activities = Lists.newArrayList();

	public String getDateDescription() {
		return dateDescription;
	}

	public void setDateDescription(String dateDescription) {
		this.dateDescription = dateDescription;
	}

	public List<ActivityRepresentation> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityRepresentation> activities) {
		this.activities = activities;
	}

	public static ActivityGroupRepresentation from(ActivityGroup activityGroup) {
		ActivityGroupRepresentation rep = new ActivityGroupRepresentation();
		rep.dateDescription = activityGroup.getDateDescription();
		rep.activities.addAll(ActivityRepresentation.from(activityGroup.getActivities()));
		return rep;
	}

	public static List<ActivityGroupRepresentation> from(List<ActivityGroup> activityGroups) {
		List<ActivityGroupRepresentation> reps = Lists.newArrayList();
		for (ActivityGroup activityGroup : activityGroups) {
			reps.add(from(activityGroup));
		}
		return reps;
	}
}
