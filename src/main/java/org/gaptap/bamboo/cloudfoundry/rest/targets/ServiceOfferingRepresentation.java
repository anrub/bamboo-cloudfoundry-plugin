/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.cloudfoundry.client.lib.domain.CloudServiceOffering;

import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
@XmlRootElement(name = "serviceOffering")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceOfferingRepresentation {

    @XmlAttribute
    private String name;
    @XmlAttribute
    private String label;
    @XmlAttribute
    private String provider;
    @XmlAttribute
    private String plan;
    @XmlAttribute
    private String version;
    
    public static List<ServiceOfferingRepresentation> from(List<CloudServiceOffering> cloudServiceOfferings){
        List<ServiceOfferingRepresentation> offerings = Lists.newArrayList();
        for(CloudServiceOffering offering: cloudServiceOfferings){
            offerings.add(from(offering));
        }
        return offerings;
    }

    public static ServiceOfferingRepresentation from(CloudServiceOffering cloudServiceOffering) {
        ServiceOfferingRepresentation service = new ServiceOfferingRepresentation();
        service.name = cloudServiceOffering.getName();
        service.provider = cloudServiceOffering.getProvider();
        service.label = cloudServiceOffering.getLabel();
        service.version = cloudServiceOffering.getVersion();
        service.plan = cloudServiceOffering.getCloudServicePlans().get(0).getName();
        return service;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
}
