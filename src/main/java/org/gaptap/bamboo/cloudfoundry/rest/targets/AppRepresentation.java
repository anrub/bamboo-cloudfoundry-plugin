/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.cloudfoundry.client.lib.domain.CloudApplication;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author David Ehringer
 * 
 */
@XmlRootElement(name = "app")
@XmlAccessorType(XmlAccessType.FIELD)
public class AppRepresentation {
    
    // TODO add buildpack?

	@XmlAttribute
	private String name;

	@XmlAttribute
	private int runningInstances;

	@XmlAttribute
	private int instances;
	
	@XmlAttribute
	private int memory;

	@XmlElement
	private List<String> uris;

	@XmlAttribute
	String state;

	@XmlElement
	private Map<String, String> resources = new HashMap<String, String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMemory() {
		return memory;
	}

	public void setMemory(int memory) {
		this.memory = memory;
	}

	public int getRunningInstances() {
		return runningInstances;
	}

	public void setRunningInstances(int runningInstances) {
		this.runningInstances = runningInstances;
	}

	public int getInstances() {
		return instances;
	}

	public void setInstances(int instances) {
		this.instances = instances;
	}

	public List<String> getUris() {
		return uris;
	}

	public void setUris(List<String> uris) {
		this.uris = uris;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Map<String, String> getResources() {
		return resources;
	}

	public void setResources(Map<String, String> resources) {
		this.resources = resources;
	}

	public static AppRepresentation from(CloudApplication cloudApp) {
		AppRepresentation app = new AppRepresentation();
		app.setName(cloudApp.getName());
		app.setMemory(cloudApp.getMemory());
		app.setInstances(cloudApp.getInstances());
		app.setRunningInstances(cloudApp.getRunningInstances());
		app.setUris(cloudApp.getUris());
		app.setState(cloudApp.getState().toString());
		// We need to convert Integers to Strings here because some CF providers
		// include resource entries like "sudo: false" and false clearly isn't
		// an integer. I'm not sure how that would even make it this far and not
		// fail in vcap_client_java but it does.
		Map<String, String> resources = Maps.newHashMap();
		for (Entry<String, Integer> resource : cloudApp.getResources().entrySet()) {
			resources.put(resource.getKey(), String.valueOf(resource.getValue()));
		}
		app.setResources(resources);
		return app;
	}

	public static List<AppRepresentation> from(List<CloudApplication> cloudApps) {
		List<AppRepresentation> apps = Lists.newArrayList();
		for (CloudApplication cloudApp : cloudApps) {
			apps.add(from(cloudApp));
		}
		return apps;
	}
}
