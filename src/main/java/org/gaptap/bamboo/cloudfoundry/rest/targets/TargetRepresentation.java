/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.rest.targets;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.gaptap.bamboo.cloudfoundry.admin.Target;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 * 
 */
@XmlRootElement(name = "target")
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetRepresentation {

	@XmlAttribute
	private String name;

	@XmlAttribute
	private int id;

	@XmlAttribute
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static TargetRepresentation from(Target target) {
		TargetRepresentation restTarget = new TargetRepresentation();
		restTarget.setId(target.getID());
		restTarget.setName(target.getName());
		restTarget.setUrl(target.getUrl());
		return restTarget;
	}

	public static List<TargetRepresentation> from(List<Target> targets) {
		List<TargetRepresentation> restTargets = Lists.newArrayList();
		for (Target target : targets) {
			restTargets.add(from(target));
		}
		return restTargets;
	}

}
