/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin.actions;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Credentials;
import org.gaptap.bamboo.cloudfoundry.admin.InUseByJobException;
import org.gaptap.bamboo.cloudfoundry.admin.Proxy;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.gaptap.bamboo.cloudfoundry.client.CloudAuthenticationException;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceException;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.gaptap.bamboo.cloudfoundry.client.InvalidTargetUrlException;
import org.gaptap.bamboo.cloudfoundry.client.Log4jLogger;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
@SuppressWarnings({ "serial", "unchecked" })
public class ManageTargetsAction extends BaseManagementAction implements GlobalAdminSecurityAware {

	private static final Logger LOG = Logger.getLogger(ManageTargetsAction.class);

	private static final int NO_PROXY = 0;

	private int targetId;
	private String url;
	private String name;
	private String description;
	private boolean trustSelfSignedCerts;
	private int credentialsId;
	private int proxyId = NO_PROXY;
	private boolean disableForBuildPlans = false;

	private List<Job> jobsUsingTarget = Lists.newArrayList();

	private final CloudFoundryAdminService adminService;
	private final CloudFoundryServiceFactory cloudFoundryClientFactory;

	public ManageTargetsAction(CloudFoundryAdminService adminService,
			CloudFoundryServiceFactory cloudFoundryClientFactory) {
		this.adminService = adminService;
		this.cloudFoundryClientFactory = cloudFoundryClientFactory;
	}

	// TODO implement a remove operation

	public String doCreate() throws Exception {
		if (!isValidTarget()) {
			setMode(ADD);
			addActionError(getText("cloudfoundry.global.errors.task.validation.errors"));
			return ERROR;
		}
		adminService.addTarget(name, url, description, trustSelfSignedCerts, credentialsId, proxyId == NO_PROXY ? null : proxyId, disableForBuildPlans);
		return SUCCESS;
	}

	public String doEdit() {
		LOG.debug("Editing target with ID: " + targetId);
		setMode(EDIT);
		Target target = adminService.getTarget(targetId);
		name = target.getName();
		url = target.getUrl();
		trustSelfSignedCerts = target.isTrustSelfSignedCerts();
		description = target.getDescription();
		if (target.getCredentials() != null) {
			credentialsId = target.getCredentials().getID();
		}
		if (target.getProxy() != null) {
			proxyId = target.getProxy().getID();
		} else {
			proxyId = NO_PROXY;
		}
		disableForBuildPlans = target.isDisableForBuildPlans();
		return EDIT;
	}

	public String doUpdate() throws Exception {
		if (!isValidTarget()) {
			setMode(EDIT);
			addActionError(getText("cloudfoundry.global.errors.task.validation.errors"));
			return ERROR;
		}
		// TODO detect issues
		adminService
				.updateTarget(targetId, name, url, description, trustSelfSignedCerts, credentialsId, proxyId == NO_PROXY ? null : proxyId, disableForBuildPlans);
		return SUCCESS;
	}

	private boolean isValidTarget() throws Exception {
		// TODO max sizes?
		if (StringUtils.isBlank(name)) {
			addFieldError("name", getText("cloudfoundry.global.required.field"));
		}
		if (StringUtils.isBlank(url)) {
			addFieldError("url", getText("cloudfoundry.global.required.field"));
		}

        // Warning, there is very similar code in ViewTargetAction that should always be
        // updated when this code is updated. Consider refactoring.
		ConnectionParameters connectionParameters = new ConnectionParameters();
		connectionParameters.setTargetUrl(url);
		connectionParameters.setTrustSelfSignedCerts(trustSelfSignedCerts);

		Credentials credentials = adminService.getCredentials(credentialsId);
		connectionParameters.setUsername(credentials.getUsername());
		connectionParameters.setPassword(credentials.getPassword());

		if (proxyId != NO_PROXY) {
			Proxy proxy = adminService.getProxy(proxyId);
			connectionParameters.setProxyHost(proxy.getHost());
			connectionParameters.setProxyPort(proxy.getPort());
		}

		// Validate that we can connect to the target using provided
		// configuration
		// TODO error handling is a PoC
		try {
			cloudFoundryClientFactory.getCloudFoundryService(connectionParameters, new Log4jLogger());
		} catch (InvalidTargetUrlException e) {
			addActionError(e.getMessage());
		} catch (CloudAuthenticationException e) {
			addActionError(e.getMessage());
		} catch (CloudFoundryServiceException e) {
			addActionError(e.getMessage());
		}
		return !(hasFieldErrors() || hasActionErrors());
	}

	public String doDelete() {
		try {
			adminService.deleteTarget(targetId);
		} catch (InUseByJobException e) {
			jobsUsingTarget.addAll(e.getJobsUsing());
			// provide data to display
			doEdit();
			return ERROR;
		}
		return SUCCESS;
	}

	public Collection<Target> getTargets() {
		return adminService.allTargets();
	}

	public Collection<Credentials> getCredentials() {
		return adminService.allCredentials();
	}

	public Collection<Proxy> getProxies() {
		return adminService.allProxies();
	}

	public Map<Integer, String> getProxyOptions() {
		Map<Integer, String> proxies = new HashMap<Integer, String>();
		proxies.put(NO_PROXY, "None");
		for (Proxy proxy : adminService.allProxies()) {
			proxies.put(proxy.getID(), proxy.getName());
		}
		return proxies;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isTrustSelfSignedCerts() {
        return trustSelfSignedCerts;
    }

    public void setTrustSelfSignedCerts(boolean trustSelfSignedCerts) {
        this.trustSelfSignedCerts = trustSelfSignedCerts;
    }

    public int getTargetId() {
		return targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}

	public int getCredentialsId() {
		return credentialsId;
	}

	public void setCredentialsId(int credentialsId) {
		this.credentialsId = credentialsId;
	}

	public int getProxyId() {
		return proxyId;
	}

	public void setProxyId(int proxyId) {
		this.proxyId = proxyId;
	}
	
	public boolean isDisableForBuildPlans() {
        return disableForBuildPlans;
    }

    public void setDisableForBuildPlans(boolean disableForBuildPlans) {
        this.disableForBuildPlans = disableForBuildPlans;
    }

    public List<Job> getJobsUsingTarget() {
		return jobsUsingTarget;
	}

	public void setJobsUsingTarget(List<Job> jobsUsingTarget) {
		this.jobsUsingTarget = jobsUsingTarget;
	}

}
