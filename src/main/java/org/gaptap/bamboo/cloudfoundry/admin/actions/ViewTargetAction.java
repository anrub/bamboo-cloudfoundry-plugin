/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin.actions;

import java.util.List;

import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.cloudfoundry.client.lib.domain.CloudInfo;
import org.cloudfoundry.client.lib.domain.CloudService;
import org.cloudfoundry.client.lib.domain.CloudServiceOffering;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Credentials;
import org.gaptap.bamboo.cloudfoundry.admin.Proxy;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.gaptap.bamboo.cloudfoundry.client.CloudAuthenticationException;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceException;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.gaptap.bamboo.cloudfoundry.client.InvalidTargetUrlException;
import org.gaptap.bamboo.cloudfoundry.client.Log4jLogger;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 * 
 */
@SuppressWarnings("serial")
public class ViewTargetAction extends BambooActionSupport {

	private int targetId;
	private Target target;
	private CloudInfo info;
	private List<CloudApplication> apps;
	private List<CloudService> services;
	private List<CloudServiceOffering> serviceConfigurations;

	private final CloudFoundryAdminService adminService;
	private final CloudFoundryServiceFactory cloudFoundryClientFactory;

	public ViewTargetAction(CloudFoundryAdminService adminService, CloudFoundryServiceFactory cloudFoundryClientFactory) {
		this.adminService = adminService;
		this.cloudFoundryClientFactory = cloudFoundryClientFactory;
	}

	public String doView() {

		target = adminService.getTarget(targetId);
		if (target == null) {
			addActionError(getText("cloudfoundry.target.notFound", Lists.newArrayList(targetId)));
			return ERROR;
		}
		// Warning, there is very similar code in ManageTargetsAction that should always be
		// updated when this code is updated. Consider refactoring.
		ConnectionParameters connectionParameters = new ConnectionParameters();
		connectionParameters.setTargetUrl(target.getUrl());
		connectionParameters.setTrustSelfSignedCerts(target.isTrustSelfSignedCerts());

		Credentials credentials = target.getCredentials();
		connectionParameters.setUsername(credentials.getUsername());
		connectionParameters.setPassword(credentials.getPassword());

		Proxy proxy = target.getProxy();
		if (proxy != null) {
			connectionParameters.setProxyHost(proxy.getHost());
			connectionParameters.setProxyPort(proxy.getPort());
		}

		CloudFoundryService client = null;
		try {
			client = cloudFoundryClientFactory.getCloudFoundryService(connectionParameters, new Log4jLogger());
		} catch (InvalidTargetUrlException e) {
			addActionError(e.getMessage());
			return ERROR;
		} catch (CloudAuthenticationException e) {
			addActionError(e.getMessage());
			return ERROR;
		} catch (CloudFoundryServiceException e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		info = client.info();
		apps = client.apps();
		services = client.services();
		serviceConfigurations = client.serviceOfferings();

		return SUCCESS;
	}

	public int getTargetId() {
		return targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public CloudInfo getInfo() {
		return info;
	}

	public void setInfo(CloudInfo info) {
		this.info = info;
	}

	public List<CloudApplication> getApps() {
		return apps;
	}

	public void setApps(List<CloudApplication> apps) {
		this.apps = apps;
	}

	public List<CloudService> getServices() {
		return services;
	}

	public void setServices(List<CloudService> services) {
		this.services = services;
	}

	public List<CloudServiceOffering> getServiceConfigurations() {
		return serviceConfigurations;
	}

	public void setServiceConfigurations(List<CloudServiceOffering> serviceConfigurations) {
		this.serviceConfigurations = serviceConfigurations;
	}

}
