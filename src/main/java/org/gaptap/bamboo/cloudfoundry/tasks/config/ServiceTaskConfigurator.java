/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import com.google.common.collect.ImmutableList;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class ServiceTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

    public static final String OPTIONS = "cf_options";
    public static final String SELECTED_OPTION = "cf_option";
    public static final String OPTION_BIND = "bind";
    public static final String OPTION_UNBIND = "unbind";
    public static final String OPTION_CREATE = "create";
    public static final String OPTION_DELETE = "delete";
    public static final String OPTION_LIST = "list";
    public static final String OPTION_SHOW = "show";

    public static final String SERVICE_CONFIGURATIONS = "cf_serviceConfigurations";
    /**
     * Format for CREATE_SERVICE: provider:_:label:_:version:_:plan
     */
    public static final String CREATE_SERVICE = "cf_createService";
    public static final String CREATE_SERVICE_HIDDEN = "cf_createServiceHidden";
    public static final String CREATE_PROVIDER = "cf_createProvider";
    public static final String CREATE_VERSION = "cf_createVersion";
    public static final String CREATE_LABEL = "cf_createLabel";
    public static final String CREATE_PLAN = "cf_createPlan";
    public static final String CREATE_NAME = "cf_createName";
    public static final String CREATE_FAIL_IF_EXISTS = "cf_createFailIfExists";

    public static final String SHOW_SERVICE_NAME = "cf_showServiceName";
    public static final String DELETE_NAME = "cf_deleteName";
    public static final String BIND_SERVICE_NAME = "cf_bindServiceName";
    public static final String BIND_APP_NAME = "cf_bindAppName";
    public static final String UNBIND_SERVICE_NAME = "cf_unbindServiceName";
    public static final String UNBIND_APP_NAME = "cf_unbindAppName";

    private static final String DEFAULT_CREATE_FAIL_IF_EXISTS = "false";

    private static final String SERVICE_CONFIG_SEPARATOR = ":_:";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SELECTED_OPTION, SHOW_SERVICE_NAME,
            CREATE_NAME, CREATE_FAIL_IF_EXISTS, DELETE_NAME, BIND_SERVICE_NAME, BIND_APP_NAME, UNBIND_SERVICE_NAME,
            UNBIND_APP_NAME);

    public ServiceTaskConfigurator(CloudFoundryAdminService adminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        super(adminService, textProvider, taskConfiguratorHelper);
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);

        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(configMap, params, FIELDS_TO_COPY);

        if (OPTION_CREATE.equals(params.getString(SELECTED_OPTION))) {
            // see format above
            String[] createService = params.getString(CREATE_SERVICE).split(SERVICE_CONFIG_SEPARATOR);
            if (createService.length == 4) {
                configMap.put(CREATE_PROVIDER, createService[0]);
                configMap.put(CREATE_LABEL, createService[1]);
                configMap.put(CREATE_VERSION, createService[2]);
                configMap.put(CREATE_PLAN, createService[3]);
            } else {
                configMap.put(CREATE_PROVIDER, null);
                configMap.put(CREATE_LABEL, null);
                configMap.put(CREATE_VERSION, null);
                configMap.put(CREATE_PLAN, null);
            }
        }
        return configMap;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(CREATE_FAIL_IF_EXISTS, DEFAULT_CREATE_FAIL_IF_EXISTS);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    private void populateContextForModify(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);

        String provider = taskDefinition.getConfiguration().get(CREATE_PROVIDER);
        if (provider != null) {
            context.put(CREATE_SERVICE, formatOfferingToken(taskDefinition.getConfiguration()));
            // Set a hidden version so that the JavaScript can appropriately set
            // the value after it dynamically loads the select list.
            context.put(CREATE_SERVICE_HIDDEN, formatOfferingToken(taskDefinition.getConfiguration()));
        }
    }

    private String formatOfferingToken(Map<String, String> configMap) {
        // see format above
        StringBuilder token = new StringBuilder();
        token.append(configMap.get(CREATE_PROVIDER));
        token.append(SERVICE_CONFIG_SEPARATOR);
        token.append(configMap.get(CREATE_LABEL));
        token.append(SERVICE_CONFIG_SEPARATOR);
        token.append(configMap.get(CREATE_VERSION));
        token.append(SERVICE_CONFIG_SEPARATOR);
        token.append(configMap.get(CREATE_PLAN));
        return token.toString();
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> options = new LinkedHashMap<String, String>();
        options.put(OPTION_BIND, textProvider.getText("cloudfoundry.task.service.option.bind"));
        options.put(OPTION_UNBIND, textProvider.getText("cloudfoundry.task.service.option.unbind"));
        options.put(OPTION_CREATE, textProvider.getText("cloudfoundry.task.service.option.create"));
        options.put(OPTION_DELETE, textProvider.getText("cloudfoundry.task.service.option.delete"));
        options.put(OPTION_LIST, textProvider.getText("cloudfoundry.task.service.option.list"));
        options.put(OPTION_SHOW, textProvider.getText("cloudfoundry.task.service.option.show"));
        context.put(OPTIONS, options);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        String option = params.getString(SELECTED_OPTION);

        if (OPTION_SHOW.equals(option)) {
            validateRequiredNotBlank(SHOW_SERVICE_NAME, params, errorCollection);
        } else if (OPTION_CREATE.equals(option)) {
            validateRequiredNotBlank(CREATE_NAME, params, errorCollection);
            validateRequiredNotBlank(CREATE_SERVICE, params, errorCollection);
            // TODO validate that token is valid as per format above
        } else if (OPTION_DELETE.equals(option)) {
            validateRequiredNotBlank(DELETE_NAME, params, errorCollection);
        } else if (OPTION_BIND.equals(option)) {
            validateRequiredNotBlank(BIND_APP_NAME, params, errorCollection);
            validateRequiredNotBlank(BIND_SERVICE_NAME, params, errorCollection);
        } else if (OPTION_UNBIND.equals(option)) {
            validateRequiredNotBlank(UNBIND_APP_NAME, params, errorCollection);
            validateRequiredNotBlank(UNBIND_SERVICE_NAME, params, errorCollection);
        }
    }
}
