/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import java.util.List;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;

import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class PushTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

    // Application
    public static final String APP_LOCATION_OPTIONS = "cf_appLocationOptions";
    public static final String APP_LOCATION_OPTION_FILE = "file";
    public static final String APP_LOCATION_OPTION_DIRECTORY = "directory";
    public static final String APP_LOCATION = "cf_appLocation";
    public static final String FILE = "cf_file";
    private static final String DEFAULT_FILE = "target/*.war";
    private static final String DEFAULT_APP_LOCATION = APP_LOCATION_OPTION_FILE;
    public static final String DIRECTORY = "cf_directory";
    public static final String START = "cf_start";
    public static final String MONITOR = "cf_monitor";
    public static final String SECONDS_TO_MONITOR = "cf_secondsToMonitor";

    private static final String START_APP_DEFAULT = "true";
    private static final String MONITOR_DEFAULT = "false";
    private static final String SECONDS_TO_MONITOR_DEFAULT = "60";

    // Application Configuration
    public static final String APP_CONFIG_OPTIONS = "cf_appConfigOptions";
    public static final String APP_CONFIG_OPTION_MANUAL = "manual";
    public static final String APP_CONFIG_OPTION_YAML = "yaml";
    public static final String SELECTED_APP_CONFIG_OPTION = "cf_appConfigOption";
    public static final String YAML_FILE = "cf_yamlFile";
    public static final String YAML_FILE_DEFAULT = "manifest.yml";
    public static final String APPLICATION_NAME = "cf_applicationName";
    public static final String URIS = "cf_uris";
    public static final String MEMORY = "cf_memory";
    public static final String DISK_QUOTA = "cf_diskQuota";
    public static final String INSTANCES = "cf_instances";
    public static final String ENVIRONMENT = "cf_environment";
    public static final String COMMAND = "cf_command";
    public static final String BUILDPACK_URL = "cf_buildpackUrl";
    public static final String STARTUP_TIMEOUT = "cf_timeout";

    // Services
    public static final String SERVICES = "cf_services";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(APPLICATION_NAME, URIS, MEMORY, DISK_QUOTA,
            INSTANCES, ENVIRONMENT, COMMAND, BUILDPACK_URL, START, MONITOR, SECONDS_TO_MONITOR, APP_LOCATION, FILE,
            DIRECTORY, SERVICES, SELECTED_APP_CONFIG_OPTION, YAML_FILE, STARTUP_TIMEOUT);

    public PushTaskConfigurator(CloudFoundryAdminService adminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        super(adminService, textProvider, taskConfiguratorHelper);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params,
            @Nullable final TaskDefinition previousTaskDefinition) {
        Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(APP_LOCATION, DEFAULT_APP_LOCATION);
        context.put(FILE, DEFAULT_FILE);
        context.put(START, START_APP_DEFAULT);
        context.put(MONITOR, MONITOR_DEFAULT);
        context.put(SECONDS_TO_MONITOR, SECONDS_TO_MONITOR_DEFAULT);
        context.put(SELECTED_APP_CONFIG_OPTION, APP_CONFIG_OPTION_MANUAL);
        context.put(YAML_FILE, YAML_FILE_DEFAULT);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> appLocationOptions = Maps.newHashMap();
        appLocationOptions.put(APP_LOCATION_OPTION_FILE,
                textProvider.getText("cloudfoundry.task.push.appLocationOptions.file"));
        appLocationOptions.put(APP_LOCATION_OPTION_DIRECTORY,
                textProvider.getText("cloudfoundry.task.push.appLocationOptions.directory"));
        context.put(APP_LOCATION_OPTIONS, appLocationOptions);

        Map<String, String> appConfigOptions = Maps.newHashMap();
        appConfigOptions.put(APP_CONFIG_OPTION_MANUAL,
                textProvider.getText("cloudfoundry.task.push.appConfigOptions.manual"));
        appConfigOptions.put(APP_CONFIG_OPTION_YAML,
                textProvider.getText("cloudfoundry.task.push.appConfigOptions.yaml"));
        context.put(APP_CONFIG_OPTIONS, appConfigOptions);
    }

    protected void populateContextForModify(@NotNull final Map<String, Object> context,
            @NotNull TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        String fileLocation = params.getString(APP_LOCATION);
        if (fileLocation.equals(APP_LOCATION_OPTION_FILE)) {
            validateRequiredNotBlank(FILE, params, errorCollection);
        } else if (fileLocation.equals(APP_LOCATION_OPTION_DIRECTORY)) {
            validateRequiredNotBlank(DIRECTORY, params, errorCollection);
        } else {
            errorCollection.addError(APP_LOCATION,
                    textProvider.getText("cloudfoundry.task.push.appLocationOptions.unknown"));
        }

        if (APP_CONFIG_OPTION_MANUAL.equals(params.getString(SELECTED_APP_CONFIG_OPTION))) {
            validateManualAppConfig(params, errorCollection);
        } else if (APP_CONFIG_OPTION_YAML.equals(params.getString(SELECTED_APP_CONFIG_OPTION))) {
            validateYamlAppConfig(params, errorCollection);
        } else {
            errorCollection.addError(SELECTED_APP_CONFIG_OPTION,
                    textProvider.getText("cloudfoundry.task.push.appConfigOptions.unknown"));
        }

        if ("true".equalsIgnoreCase(params.getString(MONITOR))) {
            try {
                int instances = Integer.parseInt(params.getString(SECONDS_TO_MONITOR));
                int minSeconds = 10;
                if (instances <= minSeconds) {
                    errorCollection.addError(SECONDS_TO_MONITOR, textProvider.getText(
                            "cloudfoundry.task.push.start.monitor.seconds.validation.tooSmall",
                            Lists.newArrayList(String.valueOf(minSeconds))));
                }
            } catch (NumberFormatException e) {
                errorCollection.addError(SECONDS_TO_MONITOR,
                        textProvider.getText("cloudfoundry.task.push.start.monitor.seconds.validation.integer"));
            }
        } else {
            params.put(SECONDS_TO_MONITOR, "0");
        }
    }

    private void validateManualAppConfig(ActionParametersMap params, ErrorCollection errorCollection) {
        validateRequiredNotBlank(APPLICATION_NAME, params, errorCollection);

        validateRequiredNotBlank(MEMORY, params, errorCollection);
        try {
            int memory = Integer.parseInt(params.getString(MEMORY));
            if (memory < 64) {
                errorCollection.addError(MEMORY,
                        textProvider.getText("cloudfoundry.task.push.memory.validation.tooSmall"));
            }
        } catch (NumberFormatException e) {
            errorCollection.addError(MEMORY, textProvider.getText("cloudfoundry.task.push.memory.validation.integer"));
        }

        String diskQuota = params.getString(DISK_QUOTA);
        if (!StringUtils.isEmpty(diskQuota)) {
            try {
                Integer.parseInt(diskQuota);
            } catch (NumberFormatException e) {
                errorCollection.addError(DISK_QUOTA,
                        textProvider.getText("cloudfoundry.task.push.diskQuota.validation.integer"));
            }
        }

        validateRequiredNotBlank(INSTANCES, params, errorCollection);
        try {
            int instances = Integer.parseInt(params.getString(INSTANCES));
            if (instances <= 0) {
                errorCollection.addError(INSTANCES,
                        textProvider.getText("cloudfoundry.task.push.instances.validation.tooSmall"));
            }
        } catch (NumberFormatException e) {
            errorCollection.addError(INSTANCES,
                    textProvider.getText("cloudfoundry.task.push.instances.validation.integer"));
        }

        String timeoutString = params.getString(STARTUP_TIMEOUT);
        if (!StringUtils.isEmpty(timeoutString)) {
            try {
                int timeout = Integer.parseInt(timeoutString);
                if (timeout <= 0) {
                    errorCollection.addError(STARTUP_TIMEOUT,
                            textProvider.getText("cloudfoundry.task.push.timeout.validation.tooSmall"));
                }
            } catch (NumberFormatException e) {
                errorCollection.addError(STARTUP_TIMEOUT,
                        textProvider.getText("cloudfoundry.task.push.timeout.validation.integer"));
            }
        }

        // TODO validate format if provided ENVIRONMENT
    }

    private void validateYamlAppConfig(ActionParametersMap params, ErrorCollection errorCollection) {
        validateRequiredNotBlank(YAML_FILE, params, errorCollection);
    }
}
