/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.*;

import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;

/**
 * @author David Ehringer
 */
public class RoutingTask extends AbstractCloudFoundryTask {

    public RoutingTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    public TaskResult doExecute(CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();

        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String option = configMap.get(SELECTED_OPTION);

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        if (OPTION_ADD_DOMAIN.equals(option)) {
            addDomain(cloudFoundry, buildLogger, configMap);
        } else if (OPTION_DELETE_DOMAIN.equals(option)) {
            deleteDomain(cloudFoundry, buildLogger, configMap);
        } else if (OPTION_ADD_ROUTE.equals(option)) {
            addRoute(cloudFoundry, buildLogger, configMap);
        } else if (OPTION_DELETE_ROUTE.equals(option)) {
            deleteRoute(cloudFoundry, buildLogger, configMap);
        } else {
            throw new TaskException("Unknown or unspecified routing management option: " + option);
        }

        return taskResultBuilder.build();
    }

    private void addDomain(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap) {
        buildLogger.addBuildLogEntry("Task: Adding new domain to space.");
        String domain = configMap.get(DOMAIN_ADD);
        cloudFoundry.addDomain(domain);
    }

    private void deleteDomain(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap) {
        buildLogger.addBuildLogEntry("Task: Deleting domain from space.");
        String domain = configMap.get(DOMAIN_DELETE);
        cloudFoundry.deleteDomain(domain);
    }

    private void addRoute(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap) {
        buildLogger.addBuildLogEntry("Task: Adding new route to space.");
        String domain = configMap.get(DOMAIN_ROUTE_ADD);
        String host = configMap.get(HOST_ROUTE_ADD);
        cloudFoundry.addRoute(domain, host);
    }

    private void deleteRoute(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap) {
        buildLogger.addBuildLogEntry("Task: Deleting route from space.");
        String domain = configMap.get(DOMAIN_ROUTE_DELETE);
        String host = configMap.get(HOST_ROUTE_DELETE);
        cloudFoundry.deleteRoute(domain, host);
    }

}
