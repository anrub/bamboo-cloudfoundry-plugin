/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_MANUAL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_YAML;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION_OPTION_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SECONDS_TO_MONITOR;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SELECTED_APP_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.YAML_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TARGET_URL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.tasks.utils.ApplicationConfigurationMapper;
import org.gaptap.bamboo.cloudfoundry.tasks.utils.YamlToApplicationConfigurationMapper;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.google.common.collect.Maps;

/**
 * A {@link TaskType} to push an application, syncing changes if it exists.
 * Equivalent to vmc push and vmc update.
 * 
 * @author David Ehringer
 */
public class PushTask extends AbstractCloudFoundryTask {

    private final ApplicationConfigurationMapper configMapper = new ApplicationConfigurationMapper();

    public PushTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext)
            throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap configMap = taskContext.getConfigurationMap();

        boolean start = Boolean.valueOf(configMap.get(START));
        int secondsToWait = Integer.parseInt(configMap.get(SECONDS_TO_MONITOR));
        File applicationFile = getApplicationFile(taskContext);

        String configOption = configMap.get(SELECTED_APP_CONFIG_OPTION);
        ApplicationConfiguration appConfig = null;
        if (APP_CONFIG_OPTION_MANUAL.equals(configOption)) {
            appConfig = configMapper.from(taskContext);
        } else if (APP_CONFIG_OPTION_YAML.equals(configOption)) {
            appConfig = extractAppConfigFromManifest(taskContext, configMap,
                    appConfig);
        } else {
            handleInvalidAppConfig(buildLogger, configOption);
        }
        buildLogger.addBuildLogEntry(appConfig.toString());

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(
                taskContext).success();
        if (start) {
            if (!cloudFoundry.push(appConfig, applicationFile, secondsToWait)) {
                taskResultBuilder.failed();
            }

        } else {
            cloudFoundry.push(appConfig, applicationFile, start);
        }

        return taskResultBuilder.build();
    }

    private ApplicationConfiguration extractAppConfigFromManifest(
            CommonTaskContext taskContext, ConfigurationMap configMap,
            ApplicationConfiguration appConfig) throws TaskException {
        String target = taskContext.getRuntimeTaskContext().get(TARGET_URL);
        String manifest = configMap.get(YAML_FILE);

        File rootDir = taskContext.getRootDirectory();
        File manifestFile = new File(rootDir, manifest);
        taskContext.getBuildLogger().addBuildLogEntry(
                "Loading application configuration from "
                        + manifestFile.getAbsolutePath());
        InputStream input = null;
        try {
            input = new FileInputStream(manifestFile);
        } catch (FileNotFoundException e) {
            String message = "Manifest file not found: "
                    + manifestFile.getAbsolutePath();
            taskContext.getBuildLogger().addErrorLogEntry(message, e);
            throw new TaskException(message, e);
        }

        BuildLoggerFacade logger = new BuildLoggerFacade(taskContext.getBuildLogger());
        YamlToApplicationConfigurationMapper yamlMapper = new YamlToApplicationConfigurationMapper(
                target, logger);
        return yamlMapper.from(input, getBambooVariables(taskContext));
    }

    private Map<String, String> getBambooVariables(CommonTaskContext taskContext) {
        Map<String, String> variables = Maps.newHashMap();
        for (VariableDefinitionContext context : taskContext.getCommonContext()
                .getVariableContext().getDefinitions().values()) {
            variables.put(context.getKey(), context.getValue());
        }
        return variables;
    }

    private void handleInvalidAppConfig(BuildLogger buildLogger,
            String configOption) throws TaskException {
        String message = "Task is configured with an invalid application configuration option: "
                + configOption
                + ". The task configuration is in an invalid state.";
        buildLogger.addErrorLogEntry(message);
        throw new TaskException(message);
    }

    private File getApplicationFile(CommonTaskContext taskContext)
            throws TaskException {
        File baseDir = taskContext.getRootDirectory();
        String location = taskContext.getConfigurationMap().get(APP_LOCATION);
        if (APP_LOCATION_OPTION_FILE.equals(location)) {
            String fileName = taskContext.getConfigurationMap().get(FILE);
            FileFinder finder = new FileFinder(baseDir);
            List<File> matching = finder.findFilesMatching(fileName);
            if (matching.size() != 1) {
                throw new TaskException("Found " + matching.size()
                        + " files matching the pattern " + fileName
                        + ". Only one matching file is expected.");
            }
            return matching.get(0);
        } else {
            String directory = taskContext.getConfigurationMap().get(DIRECTORY);
            return new File(baseDir, directory);
        }
    }

}