/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.BIND_APP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.BIND_SERVICE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_FAIL_IF_EXISTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_LABEL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_PLAN;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_PROVIDER;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_VERSION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.DELETE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_BIND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_CREATE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_LIST;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_SHOW;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_UNBIND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.SHOW_SERVICE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.UNBIND_APP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.UNBIND_SERVICE_NAME;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudfoundry.client.lib.CloudFoundryOperations;
import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.cloudfoundry.client.lib.domain.CloudService;
import org.cloudfoundry.maven.common.UiUtils;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceException;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;

/**
 * @author David Ehringer
 * 
 */
public class ServiceTask extends AbstractCloudFoundryTask {

    public ServiceTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String option = configMap.get(SELECTED_OPTION);

        CloudFoundryOperations client = getCloudFoundryClient(taskContext);
        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();
        if (OPTION_LIST.equals(option)) {
            list(cloudFoundry, client, buildLogger);
        } else if (OPTION_SHOW.equals(option)) {
            show(cloudFoundry, buildLogger, configMap);
        } else if (OPTION_CREATE.equals(option)) {
            create(cloudFoundry, configMap, taskResultBuilder, buildLogger);
        } else if (OPTION_DELETE.equals(option)) {
            delete(cloudFoundry, configMap, buildLogger);
        } else if (OPTION_BIND.equals(option)) {
            bind(cloudFoundry, configMap, buildLogger);
        } else if (OPTION_UNBIND.equals(option)) {
            unbind(cloudFoundry, configMap, buildLogger);
        } else {
            throw new TaskException("Unknown or unspecified service managment option: " + option);
        }

        return taskResultBuilder.build();
    }

    private void list(CloudFoundryService cloudFoundry, CloudFoundryOperations client, BuildLogger buildLogger) {
        buildLogger.addBuildLogEntry("Getting services...");
        List<CloudService> services = cloudFoundry.services();
        buildLogger.addBuildLogEntry("Getting services... OK");
        final List<CloudApplication> apps = cloudFoundry.apps();
        final Map<String, List<String>> servicesToApps = mapServicesToApps(services, apps);
        String[] output = UiUtils.renderServiceDataAsTable(services, servicesToApps).split("\\n");
        for (String line : output) {
            buildLogger.addBuildLogEntry(line);
        }
    }

    protected Map<String, List<String>> mapServicesToApps(List<CloudService> services, List<CloudApplication> apps) {
        Map<String, List<String>> servicesToApps = new HashMap<String, List<String>>(services.size());

        for (CloudApplication app : apps) {
            for (String serviceName : app.getServices()) {
                List<String> appNames = servicesToApps.get(serviceName);
                if (appNames == null) {
                    appNames = new ArrayList<String>();
                }
                appNames.add(app.getName());
                servicesToApps.put(serviceName, appNames);
            }
        }

        return servicesToApps;
    }

    private void show(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap) {
        String serviceName = configMap.get(SHOW_SERVICE_NAME);
        buildLogger.addBuildLogEntry(String.format("Getting service %s...", serviceName));
        CloudService service = cloudFoundry.service(serviceName);
        buildLogger.addBuildLogEntry(String.format("Getting service %s... OK", serviceName));
        if (service == null) {
            buildLogger.addBuildLogEntry("A service named " + serviceName + " does not exist.");
        } else {
            buildLogger.addBuildLogEntry("Service Information");
            buildLogger.addBuildLogEntry("- name: " + service.getName());
            buildLogger.addBuildLogEntry("- provider: " + service.getProvider());
            buildLogger.addBuildLogEntry("- label: " + service.getLabel());
            buildLogger.addBuildLogEntry("- version: " + service.getVersion());
            buildLogger.addBuildLogEntry("- plan: " + service.getPlan());
        }
    }

    private void create(CloudFoundryService cloudFoundry, ConfigurationMap configMap,
            TaskResultBuilder taskResultBuilder, BuildLogger buildLogger) {
        String name = configMap.get(CREATE_NAME);
        String provider = configMap.get(CREATE_PROVIDER);
        String label = configMap.get(CREATE_LABEL);
        String version = configMap.get(CREATE_VERSION);
        String plan = configMap.get(CREATE_PLAN);
        String failIfExists = configMap.get(CREATE_FAIL_IF_EXISTS);
        try {
            buildLogger.addBuildLogEntry(String.format("Creating service %s...", name));
            cloudFoundry.createService(name, provider, label, version, plan, Boolean.valueOf(failIfExists));
            buildLogger.addBuildLogEntry(String.format("Creating service %s... OK", name));
        } catch (CloudFoundryServiceException e) {
            buildLogger.addErrorLogEntry("Create service failed. The build will be marked as failed.");
            taskResultBuilder.failedWithError();
        }
    }

    private void delete(CloudFoundryService cloudFoundry, ConfigurationMap configMap, BuildLogger buildLogger) {
        String name = configMap.get(DELETE_NAME);
        buildLogger.addBuildLogEntry(String.format("Deleting service %s...", name));
        cloudFoundry.deleteService(name);
        buildLogger.addBuildLogEntry(String.format("Deleting service %s... OK", name));
    }

    private void bind(CloudFoundryService cloudFoundry, ConfigurationMap configMap, BuildLogger buildLogger) {
        String serviceName = configMap.get(BIND_SERVICE_NAME);
        String applicationName = configMap.get(BIND_APP_NAME);
        buildLogger.addBuildLogEntry(String.format("Binding service %s to app %s...", serviceName, applicationName));
        cloudFoundry.bindService(serviceName, applicationName);
        buildLogger.addBuildLogEntry(String.format("Binding service %s to app %s... OK", serviceName, applicationName));
    }

    private void unbind(CloudFoundryService cloudFoundry, ConfigurationMap configMap, BuildLogger buildLogger) {
        String serviceName = configMap.get(UNBIND_SERVICE_NAME);
        String applicationName = configMap.get(UNBIND_APP_NAME);
        buildLogger.addBuildLogEntry(String.format("Unbinding service %s to app %s...", serviceName, applicationName));
        cloudFoundry.unbindService(serviceName, applicationName);
        buildLogger.addBuildLogEntry(String.format("Unbinding service %s to app %s... OK", serviceName, applicationName));
    }

}
