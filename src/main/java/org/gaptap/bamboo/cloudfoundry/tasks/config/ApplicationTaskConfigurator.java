/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 * 
 */
public class ApplicationTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

	public static final String OPTIONS = "cf_options";
	public static final String SELECTED_OPTION = "cf_option";
	public static final String OPTION_LIST = "list";
	public static final String OPTION_SHOW = "show";
	public static final String OPTION_START = "start";
	public static final String OPTION_STOP = "stop";
	public static final String OPTION_RESTART = "restart";
	public static final String OPTION_DELETE = "delete";
	public static final String OPTION_MAP = "map";
	public static final String OPTION_UNMAP = "unmap";
	public static final String OPTION_RENAME = "rename";
	public static final String OPTION_APP_NAME_SEARCH = "appNameSearch";

	public static final String SHOW_NAME = "cf_showName";
	public static final String START_NAME = "cf_startName";
	public static final String STOP_NAME = "cf_stopName";
	public static final String RESTART_NAME = "cf_restartName";
	public static final String DELETE_NAME = "cf_deleteName";
	public static final String MAP_NAME = "cf_mapName";
	public static final String MAP_URI = "cf_mapUri";
	public static final String UNMAP_NAME = "cf_unmapName";
	public static final String UNMAP_URI = "cf_unmapUri";
	public static final String RENAME_NAME = "cf_renameName";
	public static final String RENAME_NEWNAME = "cf_renameNewName";
    public static final String RENAME_FAIL_IF_NOT_EXISTS = "cf_renameFailIfNotExists";
	public static final String APP_NAME_SEARCH_REGEX = "cf_appNameSearchRegex";
	public static final String APP_NAME_SEARCH_VARIABLE = "cf_appNameSearchVariable";

	private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SELECTED_OPTION, SHOW_NAME, START_NAME,
			STOP_NAME, DELETE_NAME, RESTART_NAME, MAP_NAME, MAP_URI, UNMAP_NAME, UNMAP_URI, RENAME_NAME,
			RENAME_NEWNAME, RENAME_FAIL_IF_NOT_EXISTS, APP_NAME_SEARCH_REGEX, APP_NAME_SEARCH_VARIABLE);

	public ApplicationTaskConfigurator(CloudFoundryAdminService adminService, TextProvider textProvider,
			TaskConfiguratorHelper taskConfiguratorHelper) {
		super(adminService, textProvider, taskConfiguratorHelper);
	}

	@Override
	@NotNull
	public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
			@Nullable TaskDefinition previousTaskDefinition) {
		Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);
		taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(configMap, params, FIELDS_TO_COPY);
		return configMap;
	}

	@Override
	public void populateContextForCreate(@NotNull Map<String, Object> context) {
		super.populateContextForCreate(context);
		populateContextForAll(context);
	}

	@Override
	public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
		super.populateContextForEdit(context, taskDefinition);
		populateContextForAll(context);
		populateContextForModify(context, taskDefinition);
	}

	@Override
	public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
		super.populateContextForView(context, taskDefinition);
		populateContextForAll(context);
		populateContextForModify(context, taskDefinition);
	}

	private void populateContextForModify(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
		taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
	}

	private void populateContextForAll(@NotNull final Map<String, Object> context) {
		Map<String, String> options = new LinkedHashMap<String, String>();
		options.put(OPTION_START, textProvider.getText("cloudfoundry.task.application.option.start"));
		options.put(OPTION_STOP, textProvider.getText("cloudfoundry.task.application.option.stop"));
		options.put(OPTION_RESTART, textProvider.getText("cloudfoundry.task.application.option.restart"));
		options.put(OPTION_DELETE, textProvider.getText("cloudfoundry.task.application.option.delete"));
		options.put(OPTION_LIST, textProvider.getText("cloudfoundry.task.application.option.list"));
		options.put(OPTION_SHOW, textProvider.getText("cloudfoundry.task.application.option.show"));
        options.put(OPTION_RENAME, textProvider.getText("cloudfoundry.task.application.option.rename"));
		options.put(OPTION_MAP, textProvider.getText("cloudfoundry.task.application.option.map"));
		options.put(OPTION_UNMAP, textProvider.getText("cloudfoundry.task.application.option.unmap"));
		options.put(OPTION_APP_NAME_SEARCH, textProvider.getText("cloudfoundry.task.application.option.appNameSearch"));
		context.put(OPTIONS, options);
	}

	@Override
	public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
		super.validate(params, errorCollection);

		String option = params.getString(SELECTED_OPTION);

		if (OPTION_LIST.equals(option)) {
		} else if (OPTION_SHOW.equals(option)) {
			validateRequiredNotBlank(SHOW_NAME, params, errorCollection);
		} else if (OPTION_START.equals(option)) {
			validateRequiredNotBlank(START_NAME, params, errorCollection);
		} else if (OPTION_STOP.equals(option)) {
			validateRequiredNotBlank(STOP_NAME, params, errorCollection);
		} else if (OPTION_RESTART.equals(option)) {
			validateRequiredNotBlank(RESTART_NAME, params, errorCollection);
		} else if (OPTION_DELETE.equals(option)) {
			validateRequiredNotBlank(DELETE_NAME, params, errorCollection);
		} else if (OPTION_MAP.equals(option)) {
			validateRequiredNotBlank(MAP_NAME, params, errorCollection);
			validateRequiredNotBlank(MAP_URI, params, errorCollection);
		} else if (OPTION_UNMAP.equals(option)) {
			validateRequiredNotBlank(UNMAP_NAME, params, errorCollection);
			validateRequiredNotBlank(UNMAP_URI, params, errorCollection);
		} else if (OPTION_RENAME.equals(option)) {
			validateRequiredNotBlank(RENAME_NAME, params, errorCollection);
			validateRequiredNotBlank(RENAME_NEWNAME, params, errorCollection);
		} else if (OPTION_APP_NAME_SEARCH.equals(option)) {
			validateRequiredNotBlank(APP_NAME_SEARCH_REGEX, params, errorCollection);
			validateRequiredNotBlank(APP_NAME_SEARCH_VARIABLE, params, errorCollection);
			validateRegex(params, errorCollection);
		}
	}

	private void validateRegex(ActionParametersMap params, ErrorCollection errorCollection) {
		String regex = params.getString(APP_NAME_SEARCH_REGEX);
		if (regex != null) {
			try {
				Pattern.compile(regex);
			} catch (PatternSyntaxException e) {
				errorCollection.addError(APP_NAME_SEARCH_REGEX,
						textProvider.getText("cloudfoundry.task.application.appNameSearchRegex.validation.pattern"));
			}
		}
	}
}
