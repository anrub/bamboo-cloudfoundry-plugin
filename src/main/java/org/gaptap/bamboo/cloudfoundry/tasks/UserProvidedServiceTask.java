/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.DATA_OPTION_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.DATA_OPTION_INLINE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.INLINE_DATA;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.REQUEST_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SELECTED_DATA_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;

/**
 * @author David Ehringer
 * 
 */
public class UserProvidedServiceTask extends AbstractCloudFoundryTask {

    public UserProvidedServiceTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap configMap = taskContext.getConfigurationMap();

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();

        String option = configMap.get(SELECTED_DATA_OPTION);
        Map<String, Object> credentials = null;
        if (DATA_OPTION_INLINE.equals(option)) {
            buildLogger.addBuildLogEntry("Using credentials specified inline within task configuration");
            String json = configMap.get(INLINE_DATA);
            credentials = createCredentialsFromJson(json, buildLogger);
        } else if (DATA_OPTION_FILE.equals(option)) {
            String fileName = configMap.get(REQUEST_FILE);
            File file = new File(taskContext.getWorkingDirectory(), fileName);
            buildLogger.addBuildLogEntry("Loading credentials JSON data from " + file.getAbsolutePath());
            credentials = createCredentialsFromFile(file, buildLogger);
        } else {
            throw new TaskException("Unknown or unspecified user provided service option: " + option);
        }

        String name = configMap.get(SERVICE_NAME);
        boolean ignoreIfExists = configMap.getAsBoolean(CREATE_IGNORE_IF_EXISTS);

        cloudFoundry.createUserProvidedService(name, credentials, ignoreIfExists);

        return taskResultBuilder.build();
    }

    private Map<String, Object> createCredentialsFromJson(String json, BuildLogger buildLogger) throws TaskException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
        } catch (JsonParseException e) {
            buildLogger.addErrorLogEntry("Invalid JSON", e);
            throw new TaskException("Invalid JSON");
        } catch (JsonMappingException e) {
            buildLogger.addErrorLogEntry("Invalid JSON", e);
            throw new TaskException("Invalid JSON");
        } catch (IOException e) {
            buildLogger.addErrorLogEntry("Invalid JSON", e);
            throw new TaskException("Invalid JSON");
        }
    }

    private Map<String, Object> createCredentialsFromFile(File file, BuildLogger buildLogger) throws TaskException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(new FileInputStream(file), new TypeReference<HashMap<String, Object>>() {
            });
        } catch (JsonParseException e) {
            buildLogger.addErrorLogEntry("Invalid JSON", e);
            throw new TaskException("Invalid JSON");
        } catch (JsonMappingException e) {
            buildLogger.addErrorLogEntry("Invalid JSON", e);
            throw new TaskException("Invalid JSON");
        } catch (IOException e) {
            buildLogger.addErrorLogEntry("Unable to load JSON from file.", e);
            throw new TaskException("Unable to load JSON from file.");
        }
    }

}
