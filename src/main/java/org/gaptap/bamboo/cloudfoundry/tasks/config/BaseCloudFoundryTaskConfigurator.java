/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.Maps;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 * 
 */
public class BaseCloudFoundryTaskConfigurator extends AbstractTaskConfigurator {

    // Cloud Foundry
    public static final String TARGETS = "cf_targets";
    public static final String BUILD_PLAN_TARGETS = "cf_build_targets";
    public static final String TARGET_ID = "cf_targetId";
    public static final String ORGANIZATION = "cf_org";
    public static final String SPACE = "cf_space";

    protected final CloudFoundryAdminService adminService;
    protected final TextProvider textProvider;

    public BaseCloudFoundryTaskConfigurator(CloudFoundryAdminService adminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        this.adminService = adminService;
        this.textProvider = textProvider;
        this.taskConfiguratorHelper = taskConfiguratorHelper;
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(TARGET_ID, params.getString(TARGET_ID));
        config.put(ORGANIZATION, params.getString(ORGANIZATION));
        config.put(SPACE, params.getString(SPACE));
        return config;
    }

    /**
     * If the value of the field is blank, <code>null</code> is put to the
     * configMap. Otherwise the value is put to the configMap.
     * 
     * @param configMap
     * @param params
     * @param key
     */
    protected void putValueOrNull(Map<String, String> configMap, ActionParametersMap params, String key) {
        String value = params.getString(key);
        if (StringUtils.isBlank(value)) {
            configMap.put(key, null);
        } else {
            configMap.put(key, value);
        }
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        context.put(TARGET_ID, taskDefinition.getConfiguration().get(TARGET_ID));
        context.put(ORGANIZATION, taskDefinition.getConfiguration().get(ORGANIZATION));
        context.put(SPACE, taskDefinition.getConfiguration().get(SPACE));
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context);
        context.put(TARGET_ID, taskDefinition.getConfiguration().get(TARGET_ID));
        context.put(ORGANIZATION, taskDefinition.getConfiguration().get(ORGANIZATION));
        context.put(SPACE, taskDefinition.getConfiguration().get(SPACE));
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> targets = Maps.newLinkedHashMap();
        for (Target target : adminService.allTargets()) {
            targets.put(String.valueOf(target.getID()), target.getName());
        }
        context.put(TARGETS, targets);

        Map<String, String> buildTargets = Maps.newLinkedHashMap();
        for (Target target : adminService.allTargets()) {
            if(!target.isDisableForBuildPlans()){
                buildTargets.put(String.valueOf(target.getID()), target.getName());
            }
        }
        context.put(BUILD_PLAN_TARGETS, buildTargets);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        validateRequiredNotBlank(TARGET_ID, params, errorCollection);
        validateRequiredNotBlank(ORGANIZATION, params, errorCollection);
        validateRequiredNotBlank(SPACE, params, errorCollection);
    }

    protected void validateRequiredNotBlank(String field, ActionParametersMap params, ErrorCollection errorCollection) {
        if (StringUtils.isBlank(params.getString(field))) {
            errorCollection.addError(field, textProvider.getText("cloudfoundry.global.required.field"));
        }
    }
}
