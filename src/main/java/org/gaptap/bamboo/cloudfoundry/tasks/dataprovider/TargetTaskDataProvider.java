/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.dataprovider;

import com.atlassian.bamboo.serialization.WhitelistedSerializable;
import com.atlassian.bamboo.task.RuntimeTaskDataProvider;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Proxy;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.TARGET_ID;

/**
 * Provides centrally stored Target data to the Task.
 *
 * @author David Ehringer
 */
public class TargetTaskDataProvider implements RuntimeTaskDataProvider {

    public static final String TARGET_URL = "targetUrl";
    public static final String TRUST_SELF_SIGNED_CERTS = "trustSelfSignedCerts";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String PROXY_HOST = "proxyHost";
    public static final String PROXY_PORT = "proxyPort";
    public static final String DISABLE_FOR_BUILD_PLANS = "disableForBuildPlans";

    private CloudFoundryAdminService adminService;
    private TransactionTemplate transactionTemplate;

    public TargetTaskDataProvider(CloudFoundryAdminService adminService, TransactionTemplate transactionTemplate) {
        this.adminService = adminService;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    @NotNull
    public Map<String, String> populateRuntimeTaskData(@NotNull TaskDefinition taskDefinition,
                                                       @NotNull CommonContext commonContext) {
        final String targetId = taskDefinition.getConfiguration().get(TARGET_ID);

        return transactionTemplate.execute(new TransactionCallback<Map<String, String>>() {
            @Override
            public Map<String, String> doInTransaction() {
                Map<String, String> data = new HashMap<String, String>();
                Target target = adminService.getTarget(Integer.parseInt(targetId));

                data.put(TARGET_URL, target.getUrl());
                data.put(TRUST_SELF_SIGNED_CERTS, String.valueOf(target.isTrustSelfSignedCerts()));
                data.put(USERNAME, target.getCredentials().getUsername());
                data.put(PASSWORD, target.getCredentials().getPassword());
                data.put(DISABLE_FOR_BUILD_PLANS, String.valueOf(target.isDisableForBuildPlans()));

                Proxy proxy = target.getProxy();
                if (proxy != null) {
                    data.put(PROXY_HOST, proxy.getHost());
                    data.put(PROXY_PORT, String.valueOf(proxy.getPort()));
                }
                return data;
            }
        });
    }

    @Override
    public void processRuntimeTaskData(@NotNull TaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        // Nothing to do
    }

    @Override
    public void processRuntimeTaskData(@NotNull RuntimeTaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        // Nothing to do
    }

    @Override
    public Map<String, WhitelistedSerializable> createRuntimeTaskData(@NotNull RuntimeTaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        return new HashMap<String, WhitelistedSerializable>();
    }
}
