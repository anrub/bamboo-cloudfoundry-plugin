/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class UserProvidedServiceTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

    public static final String SERVICE_NAME = "cf_servicename";
    public static final String SELECTED_DATA_OPTION = "cf_dataOption";
    public static final String DATA_OPTION_INLINE = "inline";
    public static final String DATA_OPTION_FILE = "file";
    private static final String SELECTED_DATA_OPTION_DEFAULT = DATA_OPTION_INLINE;

    public static final String REQUEST_FILE = "cf_file";
    public static final String INLINE_DATA = "cf_inlineData";
    private static final String INLINE_DATA_EXAMPLE = "{\n  \"example_name\": \"example_value\"\n}";

    public static final String CREATE_IGNORE_IF_EXISTS = "cf_createIgnoreIfExists";
    private static final boolean CREATE_IGNORE_IF_EXISTS_DEFAULT = true;

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SERVICE_NAME, SELECTED_DATA_OPTION,
            REQUEST_FILE, INLINE_DATA, CREATE_IGNORE_IF_EXISTS);

    public UserProvidedServiceTaskConfigurator(CloudFoundryAdminService adminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        super(adminService, textProvider, taskConfiguratorHelper);
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(configMap, params, FIELDS_TO_COPY);
        return configMap;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(SELECTED_DATA_OPTION, SELECTED_DATA_OPTION_DEFAULT);
        context.put(CREATE_IGNORE_IF_EXISTS, CREATE_IGNORE_IF_EXISTS_DEFAULT);
        context.put(INLINE_DATA, INLINE_DATA_EXAMPLE);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    private void populateContextForModify(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> dataOptions = Maps.newLinkedHashMap();
        dataOptions.put(DATA_OPTION_INLINE, textProvider.getText("cloudfoundry.task.userservice.data.options.inline"));
        dataOptions.put(DATA_OPTION_FILE, textProvider.getText("cloudfoundry.task.userservice.data.options.file"));
        context.put("dataOptions", dataOptions);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        validateRequiredNotBlank(SERVICE_NAME, params, errorCollection);

        if (DATA_OPTION_INLINE.equals(params.getString(SELECTED_DATA_OPTION))) {
            validateInlineJson(params, errorCollection);
        } else if (DATA_OPTION_FILE.equals(params.getString(SELECTED_DATA_OPTION))) {
            validateRequiredNotBlank(REQUEST_FILE, params, errorCollection);
        } else {
            errorCollection.addError(SELECTED_DATA_OPTION,
                    textProvider.getText("cloudfoundry.task.userservice.data.options.unknown"));
        }
    }

    private void validateInlineJson(ActionParametersMap params, ErrorCollection errorCollection) {
        validateRequiredNotBlank(INLINE_DATA, params, errorCollection);

        String json = params.getString(INLINE_DATA);
        if (!StringUtils.isBlank(json)) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
                });
            } catch (JsonParseException e) {
                errorCollection.addError(INLINE_DATA,
                        textProvider.getText("cloudfoundry.task.userservice.inline.invalid", getError(e)));
            } catch (JsonMappingException e) {
                errorCollection.addError(INLINE_DATA,
                        textProvider.getText("cloudfoundry.task.userservice.inline.invalid", getError(e)));
            } catch (IOException e) {
                errorCollection.addError(
                        INLINE_DATA,
                        textProvider.getText("cloudfoundry.task.userservice.inline.invalid",
                                new String[] { e.getMessage() }));
            }
        }
    }

    private String[] getError(JsonProcessingException e) {
        JsonLocation location = e.getLocation();
        return new String[] { "line " + location.getLineNr() + ", column " + location.getColumnNr() };
    }
}
