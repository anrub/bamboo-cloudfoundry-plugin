/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.utils;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.*;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.COMMAND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ENVIRONMENT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.INSTANCES;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.MEMORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SERVICES;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.URIS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.task.CommonTaskContext;

/**
 * @author David Ehringer
 * 
 */
public class ApplicationConfigurationMapper {

    /**
     * Assumes that data has already been validated by
     * {@link PushTaskConfigurator#validate(com.atlassian.bamboo.collections.ActionParametersMap, com.atlassian.bamboo.utils.error.ErrorCollection)}
     * 
     * @param taskContext
     * @return
     */
    public ApplicationConfiguration from(CommonTaskContext taskContext) {
        ConfigurationMap configurationMap = taskContext.getConfigurationMap();
        ApplicationConfiguration config = new ApplicationConfiguration();
        config.setName(configurationMap.get(APPLICATION_NAME));
        config.addUrls(parseCommaSeparateList(configurationMap.get(URIS)));
        if (StringUtils.isEmpty(configurationMap.get(BUILDPACK_URL))) {
            config.setBuildpackUrl(null);
        } else {
            config.setBuildpackUrl(configurationMap.get(BUILDPACK_URL));
        }
        if (StringUtils.isEmpty(configurationMap.get(COMMAND))) {
            config.setCommand(null);
        } else {
            config.setCommand(configurationMap.get(COMMAND));
        }
        config.setMemory(Integer.parseInt(configurationMap.get(MEMORY)));
        if (StringUtils.isEmpty(configurationMap.get(DISK_QUOTA))) {
            config.setDiskQuota(ApplicationConfiguration.DEFAULT_DISK_QUOTA);
        } else {
            config.setDiskQuota(Integer.parseInt(configurationMap.get(DISK_QUOTA)));
        }
        if (StringUtils.isEmpty(configurationMap.get(STARTUP_TIMEOUT))) {
            config.setStartTimeout(ApplicationConfiguration.PLATFORM_DEFAULT_TIMEOUT);
        } else {
            config.setStartTimeout(Integer.parseInt(configurationMap.get(STARTUP_TIMEOUT)));
        }
        config.setInstances(Integer.parseInt(configurationMap.get(INSTANCES)));
        config.addEnvironment(parseEnvironmentList(configurationMap.get(ENVIRONMENT)));
        config.addServiceBindings(parseCommaSeparateList(configurationMap.get(SERVICES)));
        return config;
    }

    private List<String> parseCommaSeparateList(String list) {
        list = list.trim();
        List<String> result = new ArrayList<String>();
        for (String item : list.split(",")) {
            if (item.trim().length() > 0) {
                result.add(item.trim());
            }
        }
        return result;
    }

    private Map<String, String> parseEnvironmentList(String list) {
        Map<String, String> env = new HashMap<String, String>();
        for (String line : list.split("\n")) {
            String[] var = line.trim().split("=");
            if (var.length == 2) {
                env.put(var[0].trim(), var[1].trim());
            } else if (var.length == 1) {
                if (!"".equals(var[0].trim())) {
                    env.put(var[0].trim(), "");
                }
            } else if (var.length == 0) {
                // empty, do nothing
            } else {
                // TODO Can variable names have = signs embedded in them?
                // I think we need an enhancement to handle all scenarios here.
                int index = line.indexOf('=');
                env.put(line.substring(0, index).trim(), line.substring(index + 1).trim());
            }
        }
        return env;
    }
}
