/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class RoutingTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

    public static final String OPTIONS = "cf_options";
    public static final String SELECTED_OPTION = "cf_option";
    public static final String OPTION_ADD_DOMAIN = "addDomain";
    public static final String OPTION_DELETE_DOMAIN = "deleteDomain";
    public static final String OPTION_ADD_ROUTE = "addRoute";
    public static final String OPTION_DELETE_ROUTE = "deleteRoute";

    public static final String DOMAIN_ADD = "cf_addDomain";
    public static final String DOMAIN_DELETE = "cf_deleteDomain";
    public static final String DOMAIN_ROUTE_ADD = "cf_domain_addRoute";
    public static final String HOST_ROUTE_ADD = "cf_host_addRoute";
    public static final String DOMAIN_ROUTE_DELETE = "cf_domain_deleteRoute";
    public static final String HOST_ROUTE_DELETE = "cf_host_deleteRoute";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SELECTED_OPTION, DOMAIN_ADD, DOMAIN_DELETE,
            DOMAIN_ROUTE_ADD, HOST_ROUTE_ADD, DOMAIN_ROUTE_DELETE, HOST_ROUTE_DELETE);

    public RoutingTaskConfigurator(CloudFoundryAdminService adminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        super(adminService, textProvider, taskConfiguratorHelper);
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);

        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(configMap, params, FIELDS_TO_COPY);

        return configMap;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    private void populateContextForModify(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);

    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> options = new LinkedHashMap<String, String>();
        options.put(OPTION_ADD_DOMAIN, textProvider.getText("cloudfoundry.task.route.option.addDomain"));
        options.put(OPTION_DELETE_DOMAIN, textProvider.getText("cloudfoundry.task.route.option.deleteDomain"));
        options.put(OPTION_ADD_ROUTE, textProvider.getText("cloudfoundry.task.route.option.addRoute"));
        options.put(OPTION_DELETE_ROUTE, textProvider.getText("cloudfoundry.task.route.option.deleteRoute"));
        context.put(OPTIONS, options);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        String option = params.getString(SELECTED_OPTION);

        if (OPTION_ADD_DOMAIN.equals(option)) {
            validateRequiredNotBlank(DOMAIN_ADD, params, errorCollection);
        } else if (OPTION_DELETE_DOMAIN.equals(option)) {
            validateRequiredNotBlank(DOMAIN_DELETE, params, errorCollection);
        } else if (OPTION_ADD_ROUTE.equals(option)) {
            validateRequiredNotBlank(DOMAIN_ROUTE_ADD, params, errorCollection);
            validateRequiredNotBlank(HOST_ROUTE_ADD, params, errorCollection);
        } else if (OPTION_DELETE_ROUTE.equals(option)) {
            validateRequiredNotBlank(DOMAIN_ROUTE_DELETE, params, errorCollection);
            validateRequiredNotBlank(HOST_ROUTE_DELETE, params, errorCollection);
        }
    }
}
