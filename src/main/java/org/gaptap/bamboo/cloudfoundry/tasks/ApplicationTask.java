/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.APP_NAME_SEARCH_REGEX;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.APP_NAME_SEARCH_VARIABLE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.DELETE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.MAP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.MAP_URI;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_APP_NAME_SEARCH;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_LIST;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_MAP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_RENAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_RESTART;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_SHOW;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_STOP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.OPTION_UNMAP;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_NEWNAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RENAME_FAIL_IF_NOT_EXISTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.RESTART_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.SHOW_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.START_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.STOP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.UNMAP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ApplicationTaskConfigurator.UNMAP_URI;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cloudfoundry.client.lib.domain.ApplicationStats;
import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.cloudfoundry.maven.common.UiUtils;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceException;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 * 
 */
public class ApplicationTask extends AbstractCloudFoundryTask {

    public ApplicationTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();

        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String option = configMap.get(SELECTED_OPTION);

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        if (OPTION_LIST.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Listing deployed applications.");
            list(cloudFoundry, buildLogger);
        } else if (OPTION_SHOW.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Displaying information about application.");
            show(cloudFoundry, buildLogger, configMap);
        } else if (OPTION_START.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Starting application.");
            start(cloudFoundry, configMap);
        } else if (OPTION_STOP.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Stoping application.");
            stop(cloudFoundry, configMap);
        } else if (OPTION_RESTART.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Restarting application.");
            restart(cloudFoundry, configMap);
        } else if (OPTION_DELETE.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Deleting application.");
            delete(cloudFoundry, configMap);
        } else if (OPTION_MAP.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Mapping URL to application.");
            map(cloudFoundry, configMap);
        } else if (OPTION_UNMAP.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Unmapping URL from application.");
            unmap(cloudFoundry, configMap);
        } else if (OPTION_RENAME.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Renaming application.");
            rename(cloudFoundry, configMap, taskResultBuilder, buildLogger);
        } else if (OPTION_APP_NAME_SEARCH.equals(option)) {
            buildLogger.addBuildLogEntry("Task: Searching for application name.");
            appNameSearch(cloudFoundry, taskContext, taskResultBuilder);
        } else {
            throw new TaskException("Unknown or unspecified application management option: " + option);
        }

        return taskResultBuilder.build();
    }

    private void list(CloudFoundryService cloudFoundry, BuildLogger buildLogger) {
        List<CloudApplication> apps = cloudFoundry.apps();
        String[] output = UiUtils.renderCloudApplicationsDataAsTable(apps).split("\\n");
        for (String line : output) {
            buildLogger.addBuildLogEntry(line);
        }
    }

    private void show(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap) {
        String name = configMap.get(SHOW_NAME);
        CloudApplication app = cloudFoundry.app(name);
        ApplicationStats appStats = cloudFoundry.appStats(name);
        String[] output = UiUtils.renderCloudApplicationDataAsTable(app, appStats).split("\\n");
        for (String line : output) {
            buildLogger.addBuildLogEntry(line);
        }
    }

    private void start(CloudFoundryService cloudFoundry, ConfigurationMap configMap) {
        String name = configMap.get(START_NAME);
        cloudFoundry.startApp(name);
    }

    private void stop(CloudFoundryService cloudFoundry, ConfigurationMap configMap) {
        String name = configMap.get(STOP_NAME);
        cloudFoundry.stopApp(name);
    }

    private void restart(CloudFoundryService cloudFoundry, ConfigurationMap configMap) {
        String name = configMap.get(RESTART_NAME);
        cloudFoundry.restartApp(name);
    }

    private void delete(CloudFoundryService cloudFoundry, ConfigurationMap configMap) {
        String name = configMap.get(DELETE_NAME);
        cloudFoundry.deleteApp(name);
    }

    private void map(CloudFoundryService cloudFoundry, ConfigurationMap configMap) {
        String name = configMap.get(MAP_NAME);
        String uri = configMap.get(MAP_URI);
        cloudFoundry.map(name, uri);
    }

    private void unmap(CloudFoundryService cloudFoundry, ConfigurationMap configMap) {
        String name = configMap.get(UNMAP_NAME);
        String uri = configMap.get(UNMAP_URI);
        cloudFoundry.unmap(name, uri);
    }

    private void rename(CloudFoundryService cloudFoundry, ConfigurationMap configMap,
            TaskResultBuilder taskResultBuilder, BuildLogger buildLogger) {
        String name = configMap.get(RENAME_NAME);
        String newName = configMap.get(RENAME_NEWNAME);
        String failIfNotExists = configMap.get(RENAME_FAIL_IF_NOT_EXISTS);
        try {
            cloudFoundry.renameApp(name, newName, Boolean.valueOf(failIfNotExists));
        } catch (CloudFoundryServiceException e) {
            buildLogger.addErrorLogEntry("Rename of application failed. The build will be marked as failed.");
            taskResultBuilder.failedWithError();
        }
    }

    private void appNameSearch(CloudFoundryService cloudFoundry, CommonTaskContext taskContext,
            TaskResultBuilder taskResultBuilder) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String regex = configMap.get(APP_NAME_SEARCH_REGEX);
        String variable = configMap.get(APP_NAME_SEARCH_VARIABLE);

        List<String> appNames = Lists.newArrayList();
        Pattern pattern = Pattern.compile(regex);
        for (CloudApplication app : cloudFoundry.apps()) {
            Matcher matcher = pattern.matcher(app.getName());
            if (matcher.matches()) {
                appNames.add(app.getName());
            }
        }

        if (appNames.size() == 0) {
            buildLogger.addErrorLogEntry("No application found matching regex " + regex);
            taskResultBuilder.failed();
        } else if (appNames.size() > 1) {
            StringBuilder message = new StringBuilder();
            message.append(appNames.size());
            message.append(" applications found matching regex ");
            message.append(regex);
            message.append(". Only one match is allowed. Matching applications: ");
            for (String app : appNames) {
                message.append(app);
                message.append(" ");
            }
            buildLogger.addErrorLogEntry(message.toString());
            taskResultBuilder.failed();
        } else {
            if (taskContext instanceof TaskContext) {
                Map<String, String> customBuildData = ((TaskContext) taskContext).getBuildContext().getBuildResult()
                        .getCustomBuildData();
                customBuildData.put(variable, appNames.get(0));
            } else if (taskContext instanceof DeploymentTaskContext) {
                Map<String, String> customBuildData = ((DeploymentTaskContext) taskContext).getDeploymentContext()
                        .getCurrentResult().getCustomBuildData();
                customBuildData.put(variable, appNames.get(0));
            }
        }
    }
}
