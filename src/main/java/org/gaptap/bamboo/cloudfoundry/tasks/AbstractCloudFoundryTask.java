/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;


import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ORGANIZATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.SPACE;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.DISABLE_FOR_BUILD_PLANS;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.PASSWORD;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.PROXY_HOST;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.PROXY_PORT;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TARGET_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TRUST_SELF_SIGNED_CERTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.USERNAME;

import org.cloudfoundry.client.lib.CloudFoundryOperations;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactoryVcap;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;

/**
 * @author David Ehringer
 */
public abstract class AbstractCloudFoundryTask implements CommonTaskType {

	private final CloudFoundryServiceFactory cloudFoundryServiceFactory;

	public AbstractCloudFoundryTask(EncryptionService encryptionService){
		cloudFoundryServiceFactory = new CloudFoundryServiceFactoryVcap(encryptionService);
	}
    
	@Override
    @NotNull
    public final TaskResult execute(@NotNull CommonTaskContext taskContext)
            throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
	    boolean disableForBuildPlans = Boolean.valueOf(taskContext.getRuntimeTaskContext().get(DISABLE_FOR_BUILD_PLANS));
        if(disableForBuildPlans && (taskContext instanceof TaskContext)){
            String targetUrl = taskContext.getRuntimeTaskContext().get(TARGET_URL);
            buildLogger.addErrorLogEntry("Cannot execute Task. Target " + targetUrl + " cannot be used within a Build Plan.");
            return TaskResultBuilder.newBuilder(taskContext).failedWithError().build(); 
        }
        return doExecute(taskContext);
    }
	
    protected abstract TaskResult doExecute(CommonTaskContext taskContext) throws TaskException;
    
    protected String getTarget(CommonTaskContext taskContext) {
		return taskContext.getRuntimeTaskContext().get(TARGET_URL);
	}

	@Deprecated
	protected CloudFoundryOperations getCloudFoundryClient(CommonTaskContext taskContext) throws TaskException {

		ConnectionParameters connectionParameters = createConnectionParameters(taskContext);

		BuildLogger buildLogger = taskContext.getBuildLogger();
		BuildLoggerFacade logger = new BuildLoggerFacade(buildLogger);

		String org = taskContext.getConfigurationMap().get(ORGANIZATION);
		String space = taskContext.getConfigurationMap().get(SPACE);

		return cloudFoundryServiceFactory.getClient(connectionParameters, org, space, logger);
	}

	protected CloudFoundryService getCloudFoundryService(CommonTaskContext taskContext) throws TaskException {
		BuildLoggerFacade logger = new BuildLoggerFacade(taskContext.getBuildLogger());

        String org = taskContext.getConfigurationMap().get(ORGANIZATION);
        String space = taskContext.getConfigurationMap().get(SPACE);
        
		return cloudFoundryServiceFactory.getCloudFoundryService(createConnectionParameters(taskContext), org, space, logger);
	}

	// TODO TargetResource has this same logic (implemented differently)
    // We need to combine it.
	private ConnectionParameters createConnectionParameters(CommonTaskContext taskContext) {
		String target = getTarget(taskContext);
		boolean trustSelfSignedCerts = Boolean.valueOf(taskContext.getRuntimeTaskContext().get(TRUST_SELF_SIGNED_CERTS));
		String username = taskContext.getRuntimeTaskContext().get(USERNAME);
		String password = taskContext.getRuntimeTaskContext().get(PASSWORD);
		String proxyHost = taskContext.getRuntimeTaskContext().get(PROXY_HOST);
		Integer proxyPort = null;
		if (taskContext.getRuntimeTaskContext().get(PROXY_PORT) != null) {
			proxyPort = Integer.parseInt(taskContext.getRuntimeTaskContext().get(PROXY_PORT));
		}

		ConnectionParameters connectionParameters = new ConnectionParameters();
		connectionParameters.setTargetUrl(target);
		connectionParameters.setTrustSelfSignedCerts(trustSelfSignedCerts);
		connectionParameters.setUsername(username);
		connectionParameters.setPassword(password);
		connectionParameters.setProxyHost(proxyHost);
		connectionParameters.setProxyPort(proxyPort);
		return connectionParameters;
	}
}
