[#assign curTab = "cloudfoundry" /]
<html>
<head>
    <title>[@ww.text name='dashboard.title' /]</title>
    <meta name="decorator" content="atl.dashboard"/>
    <meta name="tab" content="${curTab?html}" />
    ${webResourceManager.requireResource("org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:progress-bars")}
</head>
<body>

<style media="screen" type="text/css">
#targetList{
	float: left;
	width: 66%;
}

#targetList + div {
	float: right;
	position: relative;
	width: 32%;
}
</style>

	<div id="targetList">
	
		[@ui.bambooPanel titleKey='cloudfoundry.global.targets.list.heading']
		[#if targets!?size > 0]	
		<table id="targets" class="aui" width="100%">
			<thead><tr>
				<th class="labelPrefixCell">[@ww.text name='cloudfoundry.global.targets.list.heading.target' /]</th>
				<th class="valueCell">[@ww.text name='cloudfoundry.global.targets.list.heading.configuration' /]</th>
			</tr></thead>
			<tbody>
				[#foreach target in targets]
					<tr>
						<td class="labelPrefixCell">
							<a href="${req.contextPath}/viewTarget.action?targetId=${target.ID}">${target.name}</a><br />
							[#if target.description?has_content]
								<span class="subGrey">${target.description}</span>
							[/#if]
						</td>
						<td class="valueCell">
							[@ww.text name='cloudfoundry.global.targets.list.url' /]: <a href="${target.url}" target="_blank">${target.url}</a><br />
							[@ww.text name='cloudfoundry.global.targets.list.credentials' /]: <a id="credentials-tooltip" title="${target.credentials.username}">${target.credentials.name}</a><br />
							[#if target.proxy?has_content]
								[@ww.text name='cloudfoundry.global.targets.list.proxy' /]: <a id="proxy-tooltip" title="${target.proxy.host}:${target.proxy.port}">${target.proxy.name}</a><br />
							[/#if]   
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>			
		[#else]
			[@ui.messageBox type='warning' titleKey='cloudfoundry.dashboard.targets.none.title']
				[@ww.text name='cloudfoundry.dashboard.targets.none.message' /]
			[/@ui.messageBox]
		[/#if]
		[/@ui.bambooPanel]
	</div>
	
	<div id="recentDeployments">
	<h2>Recent Deployments</h2>
	<div id="recentlyBuilt" class="activityFeed">
		<div id="feedContainer-recentlyBuilt" class="feedContainer">
		
			[#if activityGroups!?size == 0]
				[@ui.messageBox type='info' closeable=true]
					[@ww.text name='cloudfoundry.dashboard.activity.none.message' /]
				[/@ui.messageBox]
			[/#if]
		
			[#foreach activityGroup in activityGroups]
		
				<h3 class="timestamp">${activityGroup.dateDescription}</h3>
				<ul class="activityItems">
					[#foreach activity in activityGroup.activities]
						<li class="activity-item" style="background-image: 
						[#if activity.buildState == 'Successful']
							url(${req.contextPath}/images/iconsv3/plan_successful_16.png);
						[#else]
							url(${req.contextPath}/images/iconsv3/plan_failed_16.png);
						[/#if]
						">
							<a href="${req.contextPath}/browse/${activity.planBuildKey}">
							 ${activity.projectName} &gt; ${activity.planName} &gt; #${activity.buildNumber}
							 </a>
							 &gt;
							 <a href="${req.contextPath}/browse/${activity.jobBuildKey}">
							 ${activity.jobName}
							 </a>
							 <span class="stream-build-summary stream-build-success"> 
								[#if activity.buildState == 'Successful']
									was successful
								[#else]
									failed
								[/#if]
							 </span>
							 <div class="activity-item-description">
								${activity.reasonSummary}, build took ${activity.durationDescription}
							</div>
						</li>
					[/#foreach]
				</ul>
			
			[/#foreach]
			
		</div>
	</div>
	</div>	
</body>
</html>