
[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.routing.section']

	[@ww.select labelKey="cloudfoundry.task.routing.option" name="cf_option" list=cf_options listKey="key" listValue="value" toggle='true' /]
	[@ui.bambooSection dependsOn='cf_option' showOn='addDomain']
		<p>[@ww.text name='cloudfoundry.task.routing.addDomain.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_addDomain' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='deleteDomain']
		<p>[@ww.text name='cloudfoundry.task.routing.deleteDomain.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_deleteDomain' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='addRoute']
		<p>[@ww.text name='cloudfoundry.task.routing.addRoute.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.host' name='cf_host_addRoute' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_domain_addRoute' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='deleteRoute']
		<p>[@ww.text name='cloudfoundry.task.routing.deleteRoute.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.host' name='cf_host_deleteRoute' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_domain_deleteRoute' required='true' /]
	[/@ui.bambooSection]
	
[/@ui.bambooSection]