[#ftl]
<html>
<head>
    <title>Cloud Foundry manifest.yml Help</title>
    <meta name="decorator" content="atl.popup">
</head>

<body>
	[@ui.header page="Supported Cloud Foundry Manifest File Options" /]
	<p>A subset of the <a href="http://docs.cloudfoundry.org/devguide/deploy-apps/manifest.html">manifest.yml format</a> is currently supported.</p>
	<br/>
	[@ui.bambooSection title='Minimum Fields']
	<p>At a minimum, the name field must be provided.</p>
	<br/>
	<pre class="code">
---
applications:
- name: hello-manifest
	</pre>
	<br/>
	[/@ui.bambooSection]
	[@ui.bambooSection title='All Supported Fields']
		<p>The full lists of supported configuration fields is below.</p>
		<p>When using the full service definition, if the service does not exist, it will be created for you. If it already exists, it will simply be bound to your app.</p>
		<br/>
		<pre class="code">
---
applications:
- name: hello-manifest
  instances: 1
  memory: 128M
  disk_quota: 2G
  timeout: 90
  path: .
  host: my-app
  domain: ctapps.io
  buildpack: https://github.com/cloudfoundry/heroku-buildpack-ruby.git
  command: bundle exec rake server:start_command
  env:
    greeting: hello
    BUNDLE_WITHOUT: test:development
  services:
    rediscloud-8cccc:
      label: rediscloud
      provider: garantiadata
      plan: 20mb
      version: n/a
    mongolab-6d208:
      label: mongolab
      provider: mongolab
      plan: sandbox
      version: n/a
		</pre>
	<br/>
	[/@ui.bambooSection]
	[@ui.bambooSection title='Service Bindings']
		<p>Instead of providing all service details, you may provide only the names of pre-existing services and they will be bound to the app.</p>
		<br/>
		<pre class="code">
---
applications:
- name: hello-manifest
  instances: 1
  memory: 128M
  path: .
  host: my-app
  domain: ctapps.io
  buildpack: buildpack: https://github.com/cloudfoundry/java-buildpack.git
  services:
  - rediscloud-8cccc
  - mongolab-6d208
		</pre>
	<br/>
	[/@ui.bambooSection]
	[@ui.bambooSection title='Symbol Resolution']
		<p>Most <a href="http://docs.cloudfoundry.com/docs/using/deploying-apps/manifest.html#symbols">symbol resolution</a> options are supported. The <span class="code">${r"${random-word}"}</span> and <span class="code">${r"${target-base}"}</span> special symbols are supported. The following example shows how symbols can be used.</p>
		<br/>
		<pre class="code">
---
applications:
- name: hello-manifest
  instances: 1
  memory: 128M
  host: my-${r"${name}"}-${r"${random-word}"}
  domain: ${r"${target-base}"}
  buildpack: https://github.com/cloudfoundry/${r"${name}"}-buildpack.git
  command: bin/demo/${r"${name}"}/${r"${target-base}"}/run ${r"${random-word}"}
  env:
    greeting: hello
    BUNDLE_WITHOUT: test:development
    JAVA_OPTS: "-Drandom.value=${r"${target-base}"}.${r"${random-word}"} -Dspring.profiles.active=test -Dlog.dir=/app/logs/${r"${name}"}"
    app-name: ${r"${name}"}
    e-${r"${name}"}: example
    count: ${r"${instances}"}
    size: ${r"${mem}"}
    where: ${r"${host}"}
    owner: ${r"${domain}"}
    alt-command: ${r"${command}"}
    builder: ${r"${buildpack}"}
  services:
    rediscloud-${r"${name}"}:
      label: rediscloud
      provider: garantiadata
      plan: 20mb
      version: n/a
		</pre>
		<br/>
		<p>Attributes can be placed above the "applications" block. This attributes will be made available for symbol resolution.</p>
		<br/>
		<pre class="code">
---
app-name: hello-manifest
environment: test
executable: run ${r"${app-name}"}
applications:
- name: ${r"${app-name}"}
  instances: 1
  memory: 128M
  command: bin/demo/${r"${name}"}/${r"${executable}"}
  env:
    greeting: ${r"${environment}"}
		</pre>
		<br/>
		<p>Bamboo variables can be used for symbol resolution. Global, plan/build, and deployment variables are made available.</p>
		<p>In the following example, <span class="code">appName</span> and <span class="code">environment</span> could be provided as Bamboo variables</p>
		<br/>
		<pre class="code">
---
applications:
- name: ${r"${appName}"}
  instances: 1
  memory: 128M
  env:
    greeting: ${r"${environment}"}
		</pre>
		<br/>
	[/@ui.bambooSection]
	[@ui.bambooSection title='Path Options']
		<p>The <span class="code">path: .</span> variable is allowed (as shown in the examples above) but it will be ignored in favor of the location specified in your task configuration.</p>
		<br/>
	[/@ui.bambooSection]
	[@ui.bambooSection title='Child and Multi-app Manifests']
		<p>Multi-app and child manifests are not supported.</p>
	[/@ui.bambooSection]
</body>
</html>