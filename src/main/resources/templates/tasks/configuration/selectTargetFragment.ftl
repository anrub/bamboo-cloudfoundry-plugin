

[@ui.bambooSection titleKey='cloudfoundry.task.global.target.section']

	[#if deploymentMode]
		[#if cf_targets.size() == 0]
			[@ui.messageBox type="warning" titleKey="cloudfoundry.task.global.target.none.configured.title"]
			    [@ww.text name="cloudfoundry.task.global.target.none.configured.message"]
			    [/@ww.text]&nbsp;
			    [#if fn.hasAdminPermission()]
				    [@ww.text name="cloudfoundry.task.global.target.none.configured.add"]
				        [@ww.param]${req.contextPath}/admin/cloudfoundry/configuration.action[/@ww.param]
				    [/@ww.text]
			    [/#if]
			[/@ui.messageBox]
		[/#if]
		[@ww.select labelKey="cloudfoundry.task.global.target" name="cf_targetId" required="true" list=cf_targets listKey="key" listValue="value" toggle='true' /]
	[#else]
		[#if cf_build_targets.size() == 0]
			[@ui.messageBox type="warning" titleKey="cloudfoundry.task.global.target.none.configured.build_targets.title"]
			    [@ww.text name="cloudfoundry.task.global.target.none.configured.message"]
			    [/@ww.text]&nbsp;
			    [#if fn.hasAdminPermission()]
				    [@ww.text name="cloudfoundry.task.global.target.none.configured.add"]
				        [@ww.param]${req.contextPath}/admin/cloudfoundry/configuration.action[/@ww.param]
				    [/@ww.text]
			    [/#if]
			[/@ui.messageBox]
		[/#if]
		[@ww.select labelKey="cloudfoundry.task.global.target" name="cf_targetId" required="true" list=cf_build_targets listKey="key" listValue="value" toggle='true' /]
	[/#if]
	
	[@ww.textfield labelKey='cloudfoundry.task.global.org' name='cf_org' required='true' /]
	[@ww.textfield labelKey='cloudfoundry.task.global.space' name='cf_space' required='true' /]

[/@ui.bambooSection]