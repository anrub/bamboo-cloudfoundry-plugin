<head>
    ${webResourceManager.requireResource("org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:service-task")}
</head>

[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.service.section']

	[@ww.select labelKey="cloudfoundry.task.service.option" name="cf_option" list=cf_options listKey="key" listValue="value" toggle='true' /]
	[@ui.bambooSection dependsOn='cf_option' showOn='list']
		<p>[@ww.text name='cloudfoundry.task.service.list.description' /]</p>
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='show']
		<p>[@ww.text name='cloudfoundry.task.service.show.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.showServiceName' name='cf_showServiceName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='create']
		<p>[@ww.text name='cloudfoundry.task.service.create.description' /]</p>
		[@ww.hidden name='cf_createServiceHidden' /]
		[@ww.select labelKey="cloudfoundry.task.service.createOffering" descriptionKey='cloudfoundry.task.service.createOffering.description'  name="cf_createService" list=cf_serviceOfferings listKey="key" listValue="value" toggle='true' fieldClass='service-configuration'/]
	    [@ww.textfield labelKey='cloudfoundry.task.service.createName' descriptionKey='cloudfoundry.task.service.createName.description' name='cf_createName' required='true' /]
		[@ww.checkbox labelKey='cloudfoundry.task.service.create.failTaskOnAlreadyExists' descriptionKey='cloudfoundry.task.service.create.failTaskOnAlreadyExists.description' name='cf_createFailIfExists' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='delete']
		<p>[@ww.text name='cloudfoundry.task.service.delete.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.deleteName' descriptionKey='cloudfoundry.task.service.deleteName.description' name='cf_deleteName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='bind']
		<p>[@ww.text name='cloudfoundry.task.service.bind.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.bindServiceName' name='cf_bindServiceName' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.service.bindAppName' name='cf_bindAppName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='unbind']
		<p>[@ww.text name='cloudfoundry.task.service.unbind.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.unbindServiceName' name='cf_unbindServiceName' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.service.unbindAppName' name='cf_unbindAppName' required='true' /]
	[/@ui.bambooSection]

[/@ui.bambooSection]

<script type="text/javascript">
(function () {
   var form = new BAMBOO.CLOUDFOUNDRY.ServiceForm();
   form.init();
}());
</script>
