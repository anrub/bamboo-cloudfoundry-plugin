
[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.push.application']
	[@ww.select labelKey='cloudfoundry.task.push.appLocationOptions' name='cf_appLocation'
                                        listKey='key' listValue='value' toggle='true'
                                        list=cf_appLocationOptions /]
    [@ui.bambooSection dependsOn='cf_appLocation' showOn='file']
        [@ww.textfield labelKey='cloudfoundry.task.push.fileName' name='cf_file' required='true' /]
    [/@ui.bambooSection]
    [@ui.bambooSection dependsOn='cf_appLocation' showOn='directory']
        [@ww.textfield labelKey='cloudfoundry.task.push.directory' name='cf_directory' required='true' /]
    [/@ui.bambooSection]
[/@ui.bambooSection]

[@ui.bambooSection titleKey='cloudfoundry.task.push.appConfig']

	[@ww.select labelKey='cloudfoundry.task.push.appConfigOptions' name='cf_appConfigOption'
                                        listKey='key' listValue='value' toggle='true'
                                        list=cf_appConfigOptions /]
                                        
	[@ui.bambooSection dependsOn='cf_appConfigOption' showOn='yaml']
        [@ww.textfield labelKey='cloudfoundry.task.push.yamlFile' descriptionKey='cloudfoundry.task.push.yamlFile.description' name='cf_yamlFile' required='true' /]
        [@ww.url  id='helpUrl' value='/manifestYamlHelp.action'/]
        <a href="${helpUrl}" id="yamlSubsetLink">[@ww.text name='cloudfoundry.task.push.yamlFile.description.help' /]</a>
	[/@ui.bambooSection]
	                                       
	[@ui.bambooSection dependsOn='cf_appConfigOption' showOn='manual']
		[@ww.textfield labelKey='cloudfoundry.task.global.applicationName' descriptionKey='cloudfoundry.task.push.applicationName.description' name='cf_applicationName' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.push.uris' descriptionKey='cloudfoundry.task.push.uris.description' name='cf_uris' cssClass="long-field"/]
		[@ww.textfield labelKey='cloudfoundry.task.push.instances' descriptionKey='cloudfoundry.task.push.instances.description' name='cf_instances' required='true' cssClass="short-field" /]
		[@ww.textfield labelKey='cloudfoundry.task.push.memory' descriptionKey='cloudfoundry.task.push.memory.description' name='cf_memory' required='true' cssClass="short-field" /]
		[@ww.textfield labelKey='cloudfoundry.task.push.diskQuota' descriptionKey='cloudfoundry.task.push.diskQuota.description' name='cf_diskQuota' required='false' cssClass="short-field" /]
		[@ww.textfield labelKey='cloudfoundry.task.push.buildpackUrl' descriptionKey='cloudfoundry.task.push.buildpackUrl.description' name='cf_buildpackUrl' required='false' cssClass="long-field" /]
		[@ww.textfield labelKey='cloudfoundry.task.push.command' descriptionKey='cloudfoundry.task.push.command.description' name='cf_command' required='false' cssClass="long-field" /]
		[@ww.textfield labelKey='cloudfoundry.task.push.timeout' descriptionKey='cloudfoundry.task.push.timeout.description' name='cf_timeout' required='false' cssClass="short-field" /]
	    [@ww.textarea labelKey='cloudfoundry.task.push.environment' descriptionKey='cloudfoundry.task.push.environment.description' name="cf_environment" required=false/]
		[@ww.textfield labelKey='cloudfoundry.task.push.services' descriptionKey='cloudfoundry.task.push.services.description' name='cf_services' cssClass="long-field"/]
	[/@ui.bambooSection]
[/@ui.bambooSection]

[@ui.bambooSection titleKey='cloudfoundry.task.push.start.options']
	[@ww.checkbox labelKey='cloudfoundry.task.push.start' descriptionKey='cloudfoundry.task.push.start.description' name='cf_start' toggle='true'/]
	[@ui.bambooSection dependsOn='cf_start' showOn='true']
		[@ww.checkbox labelKey='cloudfoundry.task.push.start.monitor' descriptionKey='cloudfoundry.task.push.start.monitor.description' name='cf_monitor' toggle='true' /]
		[@ui.bambooSection dependsOn='cf_monitor' showOn='true']
			[@ww.textfield labelKey='cloudfoundry.task.push.start.monitor.seconds' descriptionKey='cloudfoundry.task.push.start.monitor.seconds.description' name='cf_secondsToMonitor' required='true' cssClass="short-field" /]
		[/@ui.bambooSection]
	[/@ui.bambooSection]
[/@ui.bambooSection]

<script type="text/javascript">
 AJS.$(function() {
     AJS.$('#yamlSubsetLink').click(function(event) {
          event.preventDefault();
          openHelp(this.href);
      });
 });
 </script>