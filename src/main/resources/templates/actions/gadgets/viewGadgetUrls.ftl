<html>
<head>
    <title>[@ww.text name='cloudfoundry.gadgetUrls.heading' /]</title>
</head>

<body>

    <h1>[@ww.text name='cloudfoundry.gadgetUrls.heading' /]</h1>
    <p>[@ww.text name='cloudfoundry.gadgetUrls.description' /]</p>
    <div class="paddedClearer" ></div>

    [#macro gadgetSection gadgetKey imageLocation]
        <h2>[@ww.text name='cloudfoundry.gadgetUrls.${gadgetKey}.heading' /]</h2>
        <img class='gadgetScreenshot' src="[@cp.getStaticResourcePrefix /]${imageLocation}">
        <div class='gadgetText'>
        [@ww.text name='cloudfoundry.gadgetUrls.${gadgetKey}.description' /]<br/>
        [@ww.text name='cloudfoundry.gadgetUrls.${gadgetKey}.url']
            [@ww.param]${baseUrl}[/@ww.param]
        [/@ww.text]
        </div>
        [@ui.clear/]
    [/#macro]
    
    [@gadgetSection gadgetKey='apps' imageLocation='/download/resources/org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloud-foundry-applications-gadget/applications-thumbnail.png' /]
    [@gadgetSection gadgetKey='appStats' imageLocation='/download/resources/org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloud-foundry-application-stats-gadget/application-stats-thumbnail.png' /]
    [@gadgetSection gadgetKey='activity' imageLocation='/download/resources/org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloud-foundry-application-stats-gadget/activity-stream-thumbnail.png' /]

</body>
</html>