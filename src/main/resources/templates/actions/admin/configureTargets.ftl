[#-- @ftlvariable name="action" type="org.gaptap.bamboo.cloudfoundry.admin.actions.ManageTargetsAction" --]
[#-- @ftlvariable name="" type="org.gaptap.bamboo.cloudfoundry.admin.actions.ManageTargetsAction" --]

[#if mode == 'edit' ]
	[#assign targetAction = "/admin/cloudfoundry/updateTarget.action"]
	[#assign submitButtonKey = "cloudfoundry.admin.target.button.update"]
[#else]
	[#assign targetAction = "/admin/cloudfoundry/createTarget.action"]
	[#assign submitButtonKey = "cloudfoundry.admin.target.button.create"]
[/#if]

<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="cloudfoundry.admin.target.heading" /]
	[@ui.clear /]
	[@ww.form action=targetAction
			submitLabelKey=submitButtonKey
			titleKey='cloudfoundry.admin.target.form.title'
			cancelUri='/admin/cloudfoundry/configuration.action'
			showActionErrors='true'
			descriptionKey='cloudfoundry.admin.target.form.description']
		[@ww.hidden name='mode' /]
		[#if mode == 'edit']
			[@ww.hidden name='targetId' /]
		[/#if]
		
		[@ui.bambooSection titleKey='cloudfoundry.target.general']
		[@ww.textfield name='name' labelKey='cloudfoundry.target.name' descriptionKey='cloudfoundry.target.name.description' required='true' /]
		[@ww.textfield name='url' labelKey='cloudfoundry.target.url' descriptionKey='cloudfoundry.target.url.description' required='true' /]
		[@ww.checkbox name='trustSelfSignedCerts' labelKey='cloudfoundry.target.trustSelfSignedCerts' descriptionKey='cloudfoundry.target.trustSelfSignedCerts.description' /]
		[@ww.textfield name='description' labelKey='cloudfoundry.target.description' descriptionKey='cloudfoundry.target.description.description' required='false' /]
		
		[@ww.select labelKey='cloudfoundry.target.credentials' descriptionKey='cloudfoundry.target.credentials.description' name='credentialsId'
                                        listKey='ID' listValue='name' toggle='true'
                                        list=credentials required='true' /]
		[@ww.select labelKey='cloudfoundry.target.proxy' descriptionKey='cloudfoundry.target.proxy.description' name='proxyId'
                                        listKey='key' listValue='value' toggle='true'
                                        list=proxyOptions /]
		[/@ui.bambooSection]

		[@ui.bambooSection titleKey='cloudfoundry.target.advanced' collapsible=true isCollapsed=!disableForBuildPlans]
    		[@ww.checkbox labelKey='cloudfoundry.target.disableTargetForBuildPlans' descriptionKey='cloudfoundry.target.disableTargetForBuildPlans.description' name='disableForBuildPlans' /]
		[/@ui.bambooSection]
	[/@ww.form]
</body>
</html>
