
<head>
    <meta name="decorator" content="alt.general">
    ${webResourceManager.requireResource("org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:progress-bars")}
</head>

[@ui.header pageKey="cloudfoundry.target.info.heading" /]

<div class="aui-group">
    <div class="aui-item">
    	<img src="${req.contextPath}/download/resources/org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloudfoundry-plugin-resources/images/cloudfoundry-logo.png" style="float: left; margin-right: 5px" width="64" height="64" />
		${target.name}<br/>
		[@ww.text name='cloudfoundry.global.targets.list.url' /]: <a href="${target.url}" target="_blank">${target.url}</a><br />
		[@ww.text name='cloudfoundry.global.targets.list.credentials' /]: <a id="credentials-tooltip" title="${target.credentials.username}">${target.credentials.name}</a><br />
		[#if target.proxy?has_content]
			[@ww.text name='cloudfoundry.global.targets.list.proxy' /]: <a id="proxy-tooltip" title="${target.proxy.host}:${target.proxy.port}">${target.proxy.name}</a><br />
		[/#if]
		[#if target.disableForBuildPlans]
			[@ww.text name='cloudfoundry.global.targets.list.disableForBuildPlans' /]<br />
		[/#if]	   
    </div>
    <div class="aui-item">
    </div>
    <div class="aui-item">
    	<h2>Quotas</h2>
		<div class="aui-group">
			<div class="aui-item">Memory:</div>
			<div class="aui-item"><div id="memoryUsageBar"></div></div>
			<div class="aui-item">${info.usage.totalMemory} of ${info.limits.maxTotalMemory}</div>
		</div>
		<div class="aui-group">
			<div class="aui-item">Applications:</div>
			<div class="aui-item"><div id="applicationUsageBar"></div></div>
			<div class="aui-item">${info.usage.apps} of ${info.limits.maxApps}</div>
		</div>
		<div class="aui-group">
			<div class="aui-item">Services:</div>
			<div class="aui-item"><div id="serviceUsageBar"></div></div>
			<div class="aui-item">${info.usage.services} of ${info.limits.maxServices}</div>
		</div>
    </div>
    
</div>

<script type="text/javascript">

    AJS.$.namespace("BAMBOO.CLOUDFOUNDRY.quotas");
    
    BAMBOO.CLOUDFOUNDRY.quotas.progressBar = function(id, value, max){
    	var progressBar = AJS.$(id);
        progressBar.progressbar({ max: max, value: value });
        if(value/max > 0.9){
        	progressBar.find(".ui-progressbar-value").css({"background": '#d04437'});
        }else if(value/max > 0.75){
        	progressBar.find(".ui-progressbar-value").css({"background": '#ffd351'});
        }else{
        	progressBar.find(".ui-progressbar-value").css({"background": '#205081'});
        }
        progressBar.css({"background": '#cccccc'});
        progressBar.height(15);
    }

    AJS.$(function() {
        BAMBOO.CLOUDFOUNDRY.quotas.progressBar('#memoryUsageBar', ${info.usage.totalMemory}, ${info.limits.maxTotalMemory});
        BAMBOO.CLOUDFOUNDRY.quotas.progressBar('#applicationUsageBar', ${info.usage.apps}, ${info.limits.maxApps});
        BAMBOO.CLOUDFOUNDRY.quotas.progressBar('#serviceUsageBar', ${info.usage.services}, ${info.limits.maxServices});
        
		AJS.$("#credentials-tooltip").tooltip();
		AJS.$("#proxy-tooltip").tooltip();
    });
</script>

[@dj.tabContainer headingKeys=["cloudfoundry.target.info.basic","cloudfoundry.target.info.apps", "cloudfoundry.target.info.provisionedServices", "cloudfoundry.target.info.services", "cloudfoundry.target.info.limits"] selectedTab='${selectedTab!}']
    [@dj.contentPane labelKey="cloudfoundry.target.info.basic"]
        [@basicTab/]
    [/@dj.contentPane]
    [@dj.contentPane labelKey="cloudfoundry.target.info.apps"]
        [@appsTab/]
    [/@dj.contentPane]
    [@dj.contentPane labelKey="cloudfoundry.target.info.provisionedServices"]
        [@provisionedServicesTab/]
    [/@dj.contentPane]
    [@dj.contentPane labelKey="cloudfoundry.target.info.services"]
        [@servicesTab/]
    [/@dj.contentPane]
    [@dj.contentPane labelKey="cloudfoundry.target.info.limits"]
        [@limitsTab/]
    [/@dj.contentPane]
[/@dj.tabContainer]

[#macro basicTab]	
	<div class="form-view">
		<div class="field-group">
			<label>Name</label>
			<span class="field-value">${info.name}</span>
		</div>
		<div class="field-group">
			<label>Description</label>
			<span class="field-value">${info.description}</span>
		</div>
		<div class="field-group">
			<label>Authorization Endpoint</label>
			<span class="field-value"><a href="${info.authorizationEndpoint}" target="_blank">${info.authorizationEndpoint}</a></span>
		</div>
		<div class="field-group">
			<label>User</label>
			<span class="field-value">${info.user}</span>
		</div>
		<div class="field-group">
			<label>Support</label>
			<span class="field-value"><a href="${info.support}" target="_blank">${info.support}</a></span>
		</div>
		<div class="field-group">
			<label>Version</label>
			<span class="field-value">${info.version}</span>
		</div>
		<div class="field-group">
			<label>Build</label>
			<span class="field-value">${info.build}</span>
		</div>
	</div>
[/#macro]

[#macro appsTab]
	<p>The following applications are deployed to this Target. This includes applications from all organizations and spaces associated with this account.</p>
	<table id="apps" class="aui" width="100%">
		<thead><tr>
			<th class="labelPrefixCell"></th>
			<th class="labelPrefixCell">Application</th>
			<th class="valueCell">Configuration</th>
			<th class="valueCell">Environment</th>
			<th class="valueCell">Services</th>
			<th class="valueCell">URIs</th>
		</tr></thead>
		<tbody>	
			[#foreach app in apps]
				<tr>
					<td class="labelPrefixCell">
						[#if app.state == "STARTED"]
							<span class="aui-icon aui-icon-success">Running</span>
						[#elseif app.state == "STOPPED"]
							<span class="aui-icon aui-icon-error">Stopped</span>
						[#else]
							<span class="aui-icon aui-icon-warning">Warning</span>
						[/#if]				
					</td>
					<td class="labelPrefixCell">
						${app.name}
					</td>
					<td class="valueCell">
						State: ${app.state}					
						<br/>
						Instances: ${app.instances} (Running: ${app.runningInstances})
						[#if app.instances > app.runningInstances]
							<span class="aui-icon aui-icon-warning">Warning</span>
						[/#if]
						<br/>
						Memory: ${app.memory}<br/>
						Disk Quota: ${app.diskQuota}<br/>
						[#-- The 0.8.6 API is returning the String "null" for null values. Hopefully this is fixed as some point. --]
						[#if app.staging.command?has_content && app.staging.command != "null"]
							Command: ${app.staging.command}<br/>
						[/#if]
						[#if (app.staging.buildpackUrl)?has_content && app.staging.buildpackUrl != "null"]
							Buildpack: <a href="${app.staging.buildpackUrl}" target="blank">${app.staging.buildpackUrl}</a><br/>
						[/#if]
					</td>
					<td class="valueCell">
						[#foreach env in app.env]
							${env}<br/>
						[/#foreach]
					</td>
					<td class="valueCell">
						[#foreach service in app.services]
							${service}<br/>
						[/#foreach]
					</td>
					<td class="valueCell">
						[#foreach uri in app.uris]
							<a href="http://${uri}" target="_blank">${uri}</a><br/>
						[/#foreach]
					</td>
				</tr>
			
			[/#foreach]
		</tbody>
	</table>
[/#macro]

[#macro provisionedServicesTab]
	<p>The following services have been provisioned on this target.  This includes services from all organizations and spaces associated with this account.</p>
	<table id="apps" class="aui" width="100%">
		<thead><tr>
			<th class="labelPrefixCell">Provisioned Service</th>
			<th class="valueCell">Details</th>
		</tr></thead>
		<tbody>	
			[#foreach service in services]
				<tr>
					<td class="labelPrefixCell">
						${service.name}
					</td>
					<td class="valueCell">
						Provider: ${service.provider}<br/>
						Label: ${service.label}<br/>
						Version: ${service.version}<br/>
						Plan: ${service.plan}<br/>
					</td>
				</tr>
			
			[/#foreach]
		</tbody>
	</table>
[/#macro]

[#macro servicesTab]
	<p>The following services are available for provisioning on this target.</p>
	<table id="apps" class="aui" width="100%">
		<thead><tr>
			<th class="labelPrefixCell">Provider</th>
			<th class="valueCell">Details</th>
		</tr></thead>
		<tbody>	
			[#foreach service in serviceConfigurations]
				<tr>
					<td class="labelPrefixCell">
						${service.provider}
					</td>
					<td class="valueCell">
						Label: ${service.label}<br/>
						Version: ${service.version}<br/>
						Plans:
						[#list service.cloudServicePlans as plan]
							${plan.name}[#if plan_has_next], [/#if]
						[/#list]
					</td>
				</tr>
			
			[/#foreach]
		</tbody>
	</table>
[/#macro]

[#macro limitsTab]	
	<p>Limits for the user on this target.</p>
	<div class="form-view">
		<div class="field-group">
			<label>Max Applications</label>
			<span class="field-value">${info.limits.maxApps}</span>
		</div>
		<div class="field-group">
			<label>Max Services</label>
			<span class="field-value">${info.limits.maxServices}</span>
		</div>
		<div class="field-group">
			<label>Max Total Memory</label>
			<span class="field-value">${info.limits.maxTotalMemory}</span>
		</div>
		<div class="field-group">
			<label>Max URIs Per Application</label>
			<span class="field-value">${info.limits.maxUrisPerApp}</span>
		</div>
	</div>
[/#macro]