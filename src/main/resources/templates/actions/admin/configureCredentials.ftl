[#-- @ftlvariable name="action" type="org.gaptap.bamboo.cloudfoundry.admin.actions.ManageCredentialsAction" --]
[#-- @ftlvariable name="" type="org.gaptap.bamboo.cloudfoundry.admin.actions.ManageCredentialsAction" --]

[#if mode == 'edit' ]
	[#assign targetAction = "/admin/cloudfoundry/updateCredentials.action"]
	[#assign submitButtonKey = "cloudfoundry.admin.credentials.button.update"]
[#else]
	[#assign targetAction = "/admin/cloudfoundry/createCredentials.action"]
	[#assign submitButtonKey = "cloudfoundry.admin.credentials.button.create"]
[/#if]

<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="cloudfoundry.admin.credentials.heading" /]
	[@ui.clear /]
	[@ww.form action=targetAction
			submitLabelKey=submitButtonKey
			titleKey='cloudfoundry.admin.credentials.form.title'
			cancelUri='/admin/cloudfoundry/configuration.action'
			showActionErrors='true'
			descriptionKey='cloudfoundry.admin.credentials.form.description']
		[@ww.hidden name='mode' /]
		[#if mode == 'edit']
			[@ww.hidden name='credentialsId' /]
		[/#if]
		[@ww.textfield name='name' labelKey='cloudfoundry.credentials.name' descriptionKey='cloudfoundry.credentials.name.description' required='true' /]
		[@ww.textfield name='username' labelKey='cloudfoundry.credentials.username' descriptionKey='cloudfoundry.credentials.username.description' required='true' /]
		
		[#if password?has_content]
        	[@ww.checkbox labelKey="cloudfoundry.admin.password.change" toggle=true name="changePassword" /]
        	[@ui.bambooSection dependsOn="changePassword" showOn=true]
            	[@ww.password labelKey="cloudfoundry.credentials.password" name="password" required="true" /]
        	[/@ui.bambooSection]
    	[#else]
        	[@ww.hidden name="changePassword" value=true /]
        	[@ww.password name='password' labelKey='cloudfoundry.credentials.password' descriptionKey='cloudfoundry.credentials.password.description' required='true' /]
		 [/#if]
		
		[@ww.textfield name='description' labelKey='cloudfoundry.credentials.description' descriptionKey='cloudfoundry.credentials.description.description' required='false' /]
	[/@ww.form]
</body>
</html>
