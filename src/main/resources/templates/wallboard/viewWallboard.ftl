<html>
<head>
    <title>[@ww.text name='wallboard.title' /]</title>
${webResourceManager.requireResource("org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloudfoundry-wallbaord-resources")}
${webResourceManager.requiredResources}
    <meta name="decorator" content="none">
</head>
<body>
<div id="container"></div>
<script type="text/javascript">
(function(){

    // JSON vars
    var localAgents;
    var remoteAgents;
    var elasticAgents;
    var ec2Instances;
    var ebsVolumes;
    var queue;
    var instanceName
    var currentdate;
    var datetime;

    var srcUrl = "${context}" + "/download/resources/org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloudfoundry-wallbaord-resources/";
    var jsonUrl = "${context}" + "/getAgentDataJson.action";

    // Svg container objects
    var titleInfo;
    var localBarInfo;
    var remoteBarInfo;
    var elasticBarInfo;
    var ec2Info;
    var queueInfo;

    // Timers
    var timerRefresh;
    // Refresh rates < 5 secs bug transitions
    var timerRefreshFreq = ${secondsBeforeNextRefresh}*1000;
    var timerFlash;
    var timerFlashFreq = 800;

    // Canvas dimensions
    var defaultCanvasWidth = $("#container").width();
    var defaultCanvasHeight = 80;
    var titleCanvasHeight = 100;
    var ec2CanvasHeight = 80;
    var queueCanvasHeight = 800;

    // General Dispaly vars
    var fontFamily = "arial";
    var fontWeight = "bold";

    // If page is blocked with jsBlockUI plugin
    var blocked = false;

    // If user hasn't logged in
    var authIssue = false;

    // EC2 display vars
    var ec2StatusTypes = ["PENDING", "SHUTTING DOWN", "FAILED", "DISCONNECTED"];
    var ec2BackgroundTextSize = 13;
    var ec2Flash = false;
    var ec2Rad = 5;
    var ec2Width = 120;
    var ec2Height = 45;
    var ec2Padding = 10;
    var ec2Offset = 5;
    var ec2Margins = {"x" : 180, "y" : 0};
    var outlineOffset = 2;
    var barHeight = 45;
    var barOffset = defaultCanvasHeight - barHeight;
    var barMargins = {"x" : 180, "y" : 0};
    var agentCountHorizontalOffset = 10;

    // Title display vars
    var refreshRectWidth = (defaultCanvasWidth - ec2Margins['x']*2);
    var refreshBarHeight = 3;
    var ebsHorizontalOffset = 20;

    // Font sizes
    var ec2TextSize = 43;
    var syncTextSize = 15;
    var costTextSize = 30;
    var titleTextSize = 32;
    var labelTextSize = 15;
    var countTextSize = 45;

    // Queue display vars
    var queueFadeTime = 800;
    var queueRad = 3;
    var queueBarHeight = 20;
    var queueBarPadding = 5;
    var queueBarVerticalOffset = 50;
    var queuePlanNameSize = 14;
    var queueStatusWidth = 15;

    // Colours vars
    var colourTitleLabel = "#999";
    var colourTitleRefreshBarInitial = "green";
    var colourTitleRefreshBarFinal = "red";
    var colourTitleLastRefresh = "#333";
    var colourTitleDisconnectedEbs = "#900";

    var colourAgentBarTotal = "#f90";
    var colourAgentBarDisabled = "#333";
    var colourAgentBarIdle = "#36f";
    var colourAgentBarHung = "#900";
    var colourAgentOutline = "black"
    var colourAgentLabels = "#999";
    var colourAgentNameIdle = "#36f";
    var colourAgentNameBusy = "#f90";
    var colourAgentColons = "#666";
    var colourAgentNameDisabled = "#333";
    var colourAgentNameHung = "#900";

    var colourEc2NoduleStarting = "#36f";
    var colourEc2NoduleShutting = "#777";
    var colourEc2NoduleFailed = "#900";
    var colourEc2NoduleDisconnected = "#900";
    var colourEc2NoduleDisconnectedOutline = "#FFF";
    var colourEc2NumText = "#FFF";
    var colourEc2Outline = "#333";
    var colourEc2DescText = "#FFF";
    var colourEc2ElasticCost = "#0A0";

    var colourQueueBarOverdue = "#600";
    var colourQueueBarNormal = "#333";
    var colourQueueLabels = "#999";
    var colourQueueBarMask = "black";
    var colourQueueTime = "#999";
    var colourQueueTitle = "#999";


    // Create SVG elements
    var svgTitle = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", titleCanvasHeight)
            .attr("class", "titleCanvas");

    var svgLocal = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", defaultCanvasHeight)
            .attr("class", "localAgentCanvas");

    var svgRemote = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", defaultCanvasHeight)
            .attr("class", "remoteAgentCanvas");

    var svgElastic = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", defaultCanvasHeight)
            .attr("class", "elasticAgentCanvas");

    var svgEc2 = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", ec2CanvasHeight)
            .attr("class", "ec2Canvas");

    var svgQueue = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", queueCanvasHeight)
            .attr("class", "queueCanvas");

    // Converts a millisecs => HH:MM:SS
    function makeTimeHumanReadable(time) {

        if(time == null){return null;}

        var elapsed = time;
        var unit;
        if(time > 0) {
            elapsed = time/1000;
            if (elapsed > 60) {
                elapsed /= 60;
                if(elapsed > 60) {
                    elapsed /= 60;
                    unit = "h";
                }else{
                    unit = "m";
                }
            }else{
                unit = "s";
            }
        }

        elapsed = Math.round(elapsed);
        return elapsed + unit;
    }


    // Draws title svg elements
    function drawTitle(label, margins, canvas){
        var yPos = margins["y"];
        var xPos = margins["x"];

        // Draws title text, e.g. "Agent Status: XBAC"
        var label = canvas.selectAll("text")
                .data([label], function(d){return d;})
                .enter()
                .append('text')
                .attr("y", yPos + titleCanvasHeight - titleTextSize/2)
                .attr("x", xPos)
                .text(function(d) { return d + ": " + instanceName })
                .attr("font-size", titleTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "start")
                .attr("fill", colourTitleLabel);

        var refreshBar;

//        // Draws refresh bar"
//        var refreshBar = canvas.append('rect')
//                .attr("y", yPos + titleCanvasHeight - titleTextSize/2 - 32)
//                .attr("x", xPos)
//                .attr("height", refreshBarHeight)
//                .attr("width", 0)
//                .attr("fill", colourTitleRefreshBarInitial)
//                .attr("opacity", 0.8)
//                .attr("ry", 1)
//                .attr("rx", 1);
//
//        // Starts refresh bar
//        refreshBar.transition().duration(timerRefreshFreq*2)
//                .attr("width",refreshRectWidth)
//                .attr("fill", colourTitleRefreshBarFinal);

        // Draws last sync time
        var lastRefresh = canvas.append('text')
                .attr("y", yPos + titleCanvasHeight - syncTextSize/2)
                .attr("x", xPos)
                .attr("dy", -45)
                .text(datetime)
                .attr("font-size", syncTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "start")
                .attr("fill", colourTitleLastRefresh);



        return {"title" : label, "lastRefresh" : lastRefresh, "refreshBar" : refreshBar};
    }
    
    
   // Draws bar svg elements
    function drawAgentBar(label, dataset, barOffset, margins, canvas){
        var yPos = margins["y"];
        var xPos = margins["x"];
        var total = dataset[0] + dataset[1] + dataset[2] + dataset[3];
        var busy = dataset[0];
        var disabled = dataset[1];
        var idle = dataset[2];
        var hung = dataset[3];
        dataset[0] = total;

        var totalBarWidth = defaultCanvasWidth - 2*margins["x"];

        // Draws agent bars
        var agentBar = canvas.selectAll()
                .data(dataset)
                .enter()
                .append("rect")
                .attr("y", barOffset + yPos)
                .attr("x", function(d,i) {
                    // Total and disabled
                    if(i <= 1){
                        return xPos;
                    // Idle
                    }else if(i == 2){
                        return total == 0 ? xPos : xPos + (disabled)/total * totalBarWidth;
                     // Hung
                    }else{
                        return total == 0 ? xPos : xPos + (total - d)/total * totalBarWidth;
                    }
                })
                .attr("width", function(d,i) {
                    return total == 0 ? 0 : d/total * totalBarWidth;
                })
                .attr("height", barHeight)
                .attr("fill", function(d,i) {
                    // Total
                    if(i == 0){
                        return colourAgentBarTotal;
                    // Disabled
                    }else if(i == 1){
                        return colourAgentBarDisabled;
                    // Idle
                    }else if(i == 2){
                        return colourAgentBarIdle;
                    // Hung
                    }else{
                        return colourAgentBarHung;
                    }
                });

        // Draws rounded corner mask
        var outline = canvas.selectAll()
                .data([total])
                .enter()
                .append("rect")
                .attr("y", barOffset + yPos - outlineOffset/2)
                .attr("x", xPos - outlineOffset/2)
                .attr("width", function(d,i) {
                    return totalBarWidth + outlineOffset;
                })
                .attr("height", barHeight + outlineOffset)
                .attr("stroke-width","10")
                .attr("stroke",colourAgentOutline)
                .attr("fill-opacity", function(d,i) {
                    return "0";
                })
                .attr("opacity", function(d,i) {
                    return total == 0 ? "0" : "1";
                })
                .attr("ry", barHeight/3)
                .attr("rx", barHeight/3);

        // Draws agent titles
        var agentLabel = canvas.selectAll()
                .data([label])
                .enter()
                .append("text")
                .text(function(d){return d;})
                .attr("x", xPos)
                .attr("y", barOffset + yPos - labelTextSize)
                .attr("font-size", labelTextSize)
                .attr("text-anchor", "start")
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("fill", colourAgentLabels);

        // Draws counts (idle and busy)
        var agentCountsIdleBusy = canvas.selectAll()
                .data([idle,busy], function(d, i){
                    return label + ":" + i;
                })
                .enter()
                .append("text")
                .text(function(d){
                    if(d==0){d="";}
                    return d;
                })
                .attr("x", function(d,i){
                    // Varies the distance of the counts from the centre
                    // e.g.
                    //      XX ======= XX
                    // Increasing agentCountHorizontalOffset:
                    // XX      =======      XX
                    return  xPos + i*totalBarWidth + agentCountHorizontalOffset*((2*i) - 1);
                })
                .attr("y", barOffset + yPos)
                .attr("font-size", countTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", function(d,i){
                    if (i == 0) {
                        return "end";
                    } else {
                        return "start";
                    }
                })
                .attr("dy", -6+ countTextSize/2 + barHeight/2)
                .attr("fill", function(d,i) {
                    if(i == 0){
                        return colourAgentNameIdle;
                    }else{
                        return colourAgentNameBusy;
                    }
                })
                .style("opacity", function(d) {
                    return d == 0 ? "0" : "1";
                });

        // Draws colons
        var colons = canvas.selectAll()
                .data([":",":"], function(d, i){
                    return label + ":" + (i+2);
                })
                .enter()
                .append("text")
                .text(function(d){
                    return d;
                })
                .attr("x", function(d,i){
                    // Varies the distance of the counts from the centre.
                    // This depends on the number of chars closer to the bar (outer offset), as well as the initial
                    // horizontal offset
                    var innerCount;
                    if(i == 0){
                        innerCount = idle;
                    }else{
                        innerCount = busy;
                    }

                    var outerOffset = innerCount.toString().length * 26 + 5;
                    if(innerCount == 0){outerOffset = 0;}

                    return  xPos + i*totalBarWidth + (agentCountHorizontalOffset + outerOffset)*((2 * i) - 1);
                })
                .attr("y", barOffset + yPos)
                .attr("font-size", countTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", function(d,i){
                    if (i == 0) {
                        return "end";
                    } else {
                        return "start";
                    }
                })
                .attr("dy",  -8 + countTextSize/2 + barHeight/2)
                .attr("fill", function(d,i) {
                    return colourAgentColons;
                })
                .style("opacity", function(d,i) {
                    var retval = 0;
                    if(i == 0){
                        if(idle != 0 && disabled != 0){retval = 1;}
                    }else{
                        if(busy != 0 && hung != 0){retval = 1;}
                    }
                    return retval;
                });


        // Draws counts (disabled and hung)
        var agentCountsDisabledHung = canvas.selectAll()
                .data([disabled,hung], function(d, i){
                    return label + ":" + (i+2);
                })
                .enter()
                .append("text")
                .text(function(d){
                    if(d==0){d="";}
                    return d;
                })
                .attr("x", function(d,i){
                    var innerCount;
                    if( i == 0){
                        innerCount = idle;
                    }else{
                        innerCount = busy;
                    }

                    var outerOffset = innerCount.toString().length * 28 + 25;
                    if(innerCount == 0){outerOffset = 0;}
                    return  xPos + i*totalBarWidth + (agentCountHorizontalOffset + outerOffset)*((2 * i) - 1);
                })
                .attr("y", barOffset + yPos)
                .attr("font-size", countTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", function(d,i){
                    if (i == 0) {
                        return "end";
                    } else {
                        return "start";
                    }
                })
                .attr("dy", -6+ countTextSize/2 + barHeight/2)
                .attr("fill", function(d,i) {
                    if(i == 0){
                        return colourAgentNameDisabled;
                    }else{
                        return colourAgentNameHung;
                    }
                })
                .style("opacity", function(d) {
                    return d == 0 ? "0" : "1";
                });

        // Sets canvas size depending on whether any instances are available
        canvas.attr("height",function(){
            return total == 0 ? 0 : defaultCanvasHeight;
        });

        return {"bar" : agentBar, "agentCountsIdleBusy" : agentCountsIdleBusy, "agentCountsDisabledHung" : agentCountsDisabledHung, "outline" : outline, "colons" : colons};
    }
    
    
    
    // Fixes time display
    function fixDateDisplay(time){
        return (time < 10) ? ("0" + time) : time;
    }
    
    currentdate = new Date();
    datetime = "Last Sync: " + fixDateDisplay(currentdate.getDate()) + "/"
            + fixDateDisplay((currentdate.getMonth()+1))  + "/"
            + currentdate.getFullYear() + " "
            + fixDateDisplay(currentdate.getHours()) + ":"
            + fixDateDisplay(currentdate.getMinutes()) + ":"
            + fixDateDisplay(currentdate.getSeconds());
    
    drawTitle('Agent Status',barMargins,svgTitle);
    drawAgentBar('Local Agents', [3,4,0,0], barOffset, barMargins, svgLocal);

})();
</script>
</body>
</html>