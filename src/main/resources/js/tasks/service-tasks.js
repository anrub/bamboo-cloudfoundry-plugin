
(function($, BAMBOO) {
	
	function ServiceForm(){
		var services = [], selectedVendor, inProgressRequest;
	}
	
	ServiceForm.prototype = {
		init : function() {
			var that = this;
			this.selectedVendor = $("#cf_createServiceHidden").val();
			$("#cf_targetId").change(function() {
				that.reloadServices();
			});
			$("#cf_option").change(function() {
				that.reloadServices();
			});
			this.reloadServices();
		},
		reloadRequired : function() {
			return $('#cf_option').val() === 'create';
		},
		createKey : function(serviceConfig) {
			// format is described in ServiceTaskConfigurator
			return serviceConfig.provider + ':_:' + serviceConfig.label + ':_:' + serviceConfig.version + ':_:' + serviceConfig.plan;
		},
		reloadServices : function() {
			var that = this;
			
			// TODO check to see if reload is actually required
			
			var targetId = $("#cf_targetId").val();
			
			if(this.inProgressRequest){
				this.inProgressRequest.abort();
			}

			this.startLoading();
			this.inProgressRequest = $.ajax({
				type : "GET",
				url : AJS.contextPath() + '/rest/cloud-foundry/2.0/targets/' + targetId + '/serviceOfferings',
				dataType : "json",
				success : function(response) {
					services = response;
					var options = $("#cf_createService");
					$.each(services, function() {
						options.append($("<option />").val(that.createKey(this)).text(this.provider + ' - ' + this.label + ' (' + 
								AJS.I18n.getText('cloudfoundry.task.service.createVersion') + ': ' + this.version + ', ' + 
								AJS.I18n.getText('cloudfoundry.task.service.createPlan') + ': ' + this.plan + ')'));
					});

					that.doneLoading();
					that.attemptToSelectSameService();
					that.inProgressRequest = null;
				},
				error : function(jqXHR, textStatus, errorThrown) {
					that.doneLoading();
					if(textStatus !== 'abort'){
						that.displayErrors();
					}
					that.inProgressRequest = null;
				}
			});			
		},
		attemptToSelectSameService : function() {
			var that = this;
			$("#cf_createService option").each(function(){
				if(this.value == that.selectedVendor){
					$("#cf_createService").val(that.selectedVendor);
				}
			});
		},
		startLoading : function() {
			this.clearErrors();
			if($("#cf_serviceOptionsLoading").length == 0){
		  		$("#cf_createService").after('<span id="cf_serviceOptionsLoading" class="icon icon-loading"></span>');
				$("#cf_createService option").remove();
				var loadingOptgroup = $('<optgroup />').attr('label', 'Loading').appendTo('#cf_createService');
			}
		},
		doneLoading : function() {
			$("#cf_serviceOptionsLoading").remove();
			$("#cf_createService optgroup").remove();
		},
		displayErrors : function() {
			var errors = BAMBOO.buildFieldError([AJS.I18n.getText('cloudfoundry.task.service.target.communication.error')]);
			
			var $fieldArea = $('#cf_createService').closest(".service-configuration");
			var $description = $fieldArea.find('.description');
			errors.hide();
			if ($description.length) {
				$description.before(errors);
			} else if ($fieldArea.length) {
				$fieldArea.append(errors);
			} else {
				$('#cf_createService').after(errors);
			}
			errors.slideDown();
		},
		clearErrors : function() {
			$('#cf_createService').closest(".service-configuration").find('.error').slideUp(function() {
				$(this).remove();
			});
		}
	}
	
	BAMBOO.CLOUDFOUNDRY.ServiceForm = ServiceForm;

}(jQuery, window.BAMBOO = (window.BAMBOO || {})));